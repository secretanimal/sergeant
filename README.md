# Sergeant App for Laravel 5.2

Generates a control panel, provides the resources necessary for the application.

---

## Installation

**1 - After install Laravel framework, insert on file composer.json, inside require object this value**
```
"animaldevs/rentsergeant": "~1.0"

```
execute on console:
```
composer update
```

**2 - Register service provider, on file config/app.php add to providers array**
```
/*
 * Thirdparty Application Service Providers...
 */
Maatwebsite\Excel\ExcelServiceProvider::class,

/*
 * Sergeant Application Service Providers...
 */
Rent\Sergeant\SergeantServiceProvider::class,
```

**3 - Execute publish command**
```
php artisan vendor:publish
```

**4 - Execute optimize command load new classes**
```
php artisan optimize
```

**5 - And execute migrations and seed database**
```
php artisan migrate
php artisan db:seed --class="SergeantTableSeeder"
```

**6 - Execute command to load all updates**
```
php artisan migrate --path=database/migrations/updates
```

**7 - Register alias, on file config/app.php add to aliases array**
```
'Miscellaneous'	=> Rent\Sergeant\Libraries\Miscellaneous::class,

```

**8 - Register middlewares auth.sergeant, locale.sergeant and permission.sergeant on file app/Http/Kernel.php add to routeMiddleware array**
```
'sergeant.auth' 	        => \Rent\Sergeant\Middleware\Authenticate::class,
'sergeant.locale'         => \Rent\Sergeant\Middleware\Locale::class,
'sergeant.permission' 	=> \Rent\Sergeant\Middleware\Permission::class,
'sergeant.https'          => \Rent\Sergeant\Middleware\HttpsProtocol::class,

```

also you must to add inside $middlewareGroups array this values:
```
'noCsrWeb' => [
    \App\Http\Middleware\EncryptCookies::class,
    \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
    \Illuminate\Session\Middleware\StartSession::class,
    \Illuminate\View\Middleware\ShareErrorsFromSession::class,
],

'sergeant' => [
    \Rent\Sergeant\Middleware\Authenticate::class,
    \Rent\Sergeant\Middleware\Locale::class,
    \Rent\Sergeant\Middleware\Permission::class,
],
```

**9 - Register cron command on file app/Console/Kernel.php add to $commands array**

```
\Rent\Sergeant\Commands\Cron::class,

```

**10 - Change on file config/mail.php this line**

```
'from' => ['address' => null, 'name' => null],
```

for that

```
'from' => ['address' => env('MAIL_ADDRESS'), 'name' => env('MAIL_NAME')],
```

**11 - include this arrays in config/auth.php**

Inside guards array
```
'sergeant' => [
    'driver'    => 'session',
    'provider'  => 'sergeantUser',
],
```

Inside providers array
```
'sergeantUser' => [
    'driver'    => 'eloquent',
    'model'     => Rent\Sergeant\Models\User::class,
],
```

Inside passwords array
```
'sergeantPasswordBroker' => [
    'provider'  => 'sergeantUser',
    'email'     => 'sergeant::emails.password',
    'table'     => '001_021_password_resets',
    'expire'    => 60,
],
```

**12 - Config .env file with your database parameters connections and this example parameters**
```
APP_URL=http://mydomain.local
APP_LOG=daily

MAIL_ADDRESS=info@mydomain.local
MAIL_NAME="MY DOMAIN"
```

**13 - When the installation is complete you can access these data**
```
url: http://www.your-domain.com/sergeant
user: admin@sergeant.local
pasword: 123456
```

## Cron task
To implement the cron system must follow the following steps:


### set cron on our server

Para ello necesitaremos instanciar en nuestro servidor una única tarea cron que se encargará de revisar si tiene que disparar algún comando, normalmente con el comando /usr/bin/php y apuntando 
a la ruta absoluta del fichero artisan que se debe de encontrar en la raiz de nuestro proyecto web.
La opción -q es para evitar escritura por consola del cron

```
* * * * * /usr/bin/php -q /ruta/absoluta/a/nuestra/carpeta/raiz/artisan cron
``` 

Para editar nuestro fichero crontab para añadir la tarea, podemos hacerlo con el siguiente comando
```
# crontab -e
```

O si queremos editar el crontab de un usuario en concreto
```
# crontab -e -u usertoedit
```

### Nuestra primera tarea cron

Desde el apartado Tareas Cron podremos configurar las tareas necesarias que nuestro panel requiera ejecutar, nos encontraremos los siguientes campos:

Nombre: Descripción de la tarea cron.

Módulo: Módulo al que pertenece la tarea cron que vamos a sentenciar.

Expresión Cron: 
Periodicidad de cada tarea mediante una expresión que representará el tiempo de ejecución:

```
    *    *    *    *    *    *
    -    -    -    -    -    -
    |    |    |    |    |    |
    |    |    |    |    |    + Año [opcional]
    |    |    |    |    +----- día de la semana (0 - 7) (Sunday=0 or 7)
    |    |    |    +---------- mes (1 - 12)
    |    |    +--------------- día del mes (1 - 31)
    |    +-------------------- hora (0 - 23)
    +------------------------- minuto (0 - 59)

```

Activa: Indicamos si nuestra tarea queremos que está activa o no.

Key: Código de tarea a ejecutar, este código lo instanciamos nosotros mismos en el fichero src/config/cron.php que contiene un array de claves y fuciones

```
    return array(
    //Cron alarmas Vinipad Sales Force
    '01'       => function() { 
                            \Sergeant\Sergeant\Libraries\Cron::llamadaCron(); 
                        }
);
```
En este caso, instanciaríamos con 01 la key, si queremos que ejecute el método llamadaCron() de la clase estática Cron.