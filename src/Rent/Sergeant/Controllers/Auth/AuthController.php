<?php namespace Rent\Sergeant\Controllers\Auth;

use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Sergeant\Support\Facades\Config;
use Rent\Sergeant\Libraries\AclLibrary;
use Rent\Sergeant\Models\Package;
use Rent\Sergeant\Models\Lang;
use Rent\Sergeant\Models\User;
use Rent\Sergeant\Models\Plaza;
use Rent\Sergeant\Models\UserPlaza;
use Rent\Sergeant\Models\Plan;
use Mail;
use Input;
use Illuminate\Support\Facades\Validator;
use Request;
use DB;
class AuthController extends Controller
{

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/
    
	use AuthenticatesAndRegistersUsers, ThrottlesLogins;
        
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
        
    protected $redirectTo;
    /**
     * Route to get login form
     *
     * @var string
     */
    protected $loginPath;
    
     /**
     * Route to get register form
     *
     * @var string
     */
    protected $registerPath;
    
    /**
     * Here you can customize your guard, this guar has to set in auth.php config
     *
     * @var string
     */
    protected $guard;


	/**
	 * Create a new authentication controller instance.
	 */
	public function __construct()
	{
        $this->redirectTo   = route('dashboard');
        $this->loginPath    = route('sergeantGetLogin');
        $this->registerPath    = route('sergeantGetRegister');
	}

    /**
     * Return view with login form.
     *
     * @return \Illuminate\Http\Response
     */
        public function getLogin()
        {
            return view('sergeant::auth.login');
        }
    /**
         * Return view with register form.
         *
         * @return \Illuminate\Http\Response
         */
        public function getRegister()
        {
            $plan = Plan::lists('name_033', 'id_033');
            return view('sergeant::auth.register', compact('plan'));
        }
        
      /**
         * Return view with register form.
         *
         * @return \Illuminate\Http\Response
         */
        public function getReset()
        {
            return view('sergeant::auth.get_reset');
        }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(\Illuminate\Http\Request $request)
    {
        $this->validate($request, [
            'user_010' => 'required', 'password' => 'required',
        ]);

        $credentials = $request->only('user_010', 'password');

        if(auth('sergeant')->attempt($credentials, $request->has('remember')))
        {
            // check if user has access
            if(!auth('sergeant')->user()->access_010)
            {
                auth('sergeant')->logout();
                return redirect($this->loginPath)
                    ->withInput($request->only('user_010', 'remember'))
                    ->withErrors([
                        'loginErrors' => 3
                    ]);
            }

            // set user access control list
            session(['userAcl' => AclLibrary::getProfileAcl(auth('sergeant')->user()->profile_id_010)]);

            // check if user has permission to access
            if (!is_allowed('sergeant', 'access'))
            {
                auth('sergeant')->logout();
                return redirect($this->loginPath)
                    ->withInput($request->only('user_010', 'remember'))
                    ->withErrors([
                        'loginErrors' => 2
                    ]);
            }

            session(['packages' => Package::getRecords(['active_012' => true, 'orderBy' => ['column' => 'sorting_012', 'order' => 'desc']])]);
            session(['baseLang' => Lang::getBaseLang()]);

            return redirect()->intended($this->redirectPath());
        }

        return redirect($this->loginPath)
            ->withInput($request->only('user_010', 'remember'))
            ->withErrors([
                'loginErrors' => 1
            ]);
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        auth('sergeant')->logout();
        session()->flush();
        return redirect(config('sergeant.appName'));
    }
    
    public function store()
    {
        $rules = [
            'plan' => 'required',
            'name_029' => 'required|min:3',
            'name_010' => 'required|min:3',
            'surname_010' => 'required|min:3',
            'email_010' => 'required|email|unique:001_010_user',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password'
        ];

        $validator = Validator::make(Request::all(), $rules);
       
        if($validator->fails())
        {
            return redirect($this->registerPath)
                    ->withInput(Request::all())
                    ->withErrors([
                        'registerErrors' => 1
                    ]);
        }
        
        $confirmation_code  = array( 'confirmation_code' => str_random(32));
        
        /*Para enviar por correo*/
        $email=Input::get('email_010');
        $name=Input::get('name_010');
        
        $user = User::create([
            'name_010' => Input::get('name_010'),
            'surname_010' => Input::get('surname_010'),
            'email_010' => Input::get('email_010'),
            'user_010' => Input::get('email_010'),
            'password_010' => bcrypt(Input::get('password')),
            'confirmation_code_010' => $confirmation_code['confirmation_code'],
            'lang_id_010' => 'es',
	    'avatar_010' => '/img/avatar/user.png',
            'confirmed_010' => 0,
            'in_active_010'    => 0,
            'profile_id_010'    => 2,
            'access_010'    => 1
        ]);

        $plaza = Plaza::create([
            'name_029' => Input::get('name_029'),
            'user_id_029' => $user->id_010
        ]);
        

        DB::table('001_036_plaza_users_access')->insert([
            ['id_010' => $user->id_010, 'id_029' => $plaza->id_029]
        ]);
        
         DB::table('001_031_plan_user')->insert([
            ['id_010' => $user->id_010, 'id_033' => Input::get('plan'), 'active_031' => 1]
        ]);
                
        try{
            Mail::send('sergeant::email.verify', array('confirmation_code' =>$confirmation_code, 'name' =>$name), 
                function($message) use ($email, $name){
                       $message->from('no-reply@rentsergeant.com', 'RentSergeant');
                       $message->to($email, $name)->subject('Verifique su dirección de Email');
                       }
                );
        }catch(\Exception $e){
            //return $e;
            return  redirect($this->loginPath)->with('status', 'register_error');

        }
			
         return redirect($this->loginPath)
                    ->with('status', 'register_success'); 
    }
    
    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode_010($confirmation_code)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed_010 = 1;
        $user->in_active_010 = 1;
        $user->confirmation_code_010 = null;
        $user->save();

        return redirect($this->loginPath)
                    ->with('status', 'confirm_status'); 
        
    }
    
}