<?php namespace Rent\Sergeant\Controllers;

use Rent\Sergeant\Core\Controller;
use Rent\Sergeant\Models\Currency;

/**
 * Class CurrencyController
 * @package Rent\Sergeant\Controllers
 */

class CurrencyController extends Controller
{
    protected $routeSuffix  = 'currency';
    protected $folder       = 'currency';
    protected $package      = 'sergeant';
    protected $indexColumns = ['id_030', 'alphabetic_code_030', 'numeric_code_030', 'name_030', 'simbol_030'];
    protected $nameM        = 'name_030';
    protected $model        = Currency::class;
    protected $icon         = 'fa fa-usd';
    protected $objectTrans  = 'currency';

   public function store()
    {
        Currency::create([
            'alphabetic_code_030' => $this->request->input('alphabetic_code_030'), 
            'numeric_code_030'  => $this->request->input('numeric_code_030'), 
            'name_030' => $this->request->input('name_030'),
            'simbol_030' => $this->request->input('simbol_030')            
        ]);
       return redirect()->route($this->routeSuffix)->with([
            'msg'        => 1,
            'txtMsg'     => trans('sergeant::sergeant.message_create_record_successful', [
            'name' => $this->request->input('name_030')
            ])
        ]); 
    }
    
    public function update($parameters)
    {
        $currency = [
            'alphabetic_code_030' => $this->request->input('alphabetic_code_030'), 
            'numeric_code_030'  => $this->request->input('numeric_code_030'), 
            'name_030' => $this->request->input('name_030'),
            'simbol_030' => $this->request->input('simbol_030') ,
        ];
        
        Currency::where('id_030',$parameters)->update($currency);

        return redirect()->route($this->routeSuffix)->with([
            'msg'        => 1,
            'txtMsg'     => trans('sergeant::sergeant.message_update_record', [
            'name' => $this->request->input('name_030')
            ])
        ]); 
    }
}