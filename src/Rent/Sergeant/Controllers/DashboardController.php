<?php namespace Rent\Sergeant\Controllers;

use Rent\Sergeant\Core\Controller;

/**
 * Class DashboardController
 * @package Rent\Sergeant\Controllers
 */

class DashboardController extends Controller
{
    protected $folder       = 'dashboard';
    protected $package      = 'sergeant';
    
    public function index()
    {
        $data['package']  = $this->package;
        $data['folder']   = $this->folder;
        if(auth('sergeant')->user()->profile_id_010 == 1)
            return view('sergeant::dashboard.index', $data);
        else
            return view('catalogue::dashboard.index', $data);
    }
}