<?php namespace Rent\Sergeant\Controllers;

use Rent\Sergeant\Core\Controller;
use Rent\Sergeant\Models\Plan;
use Rent\Sergeant\Models\Currency;

/**
 * Class PlanController
 * @package Rent\Sergeant\Controllers
 */

class PlanController extends Controller
{
    protected $routeSuffix  = 'plan';
    protected $folder       = 'plan';
    protected $package      = 'sergeant';
    protected $indexColumns = ['id_033', 'name_033', 'price_033', 'alph_code_currency_033', 'description_033'];
    protected $nameM        = 'name_033';
    protected $model        = Plan::class;
    protected $icon         = 'fa fa-credit-card';
    protected $objectTrans  = 'plan';

   public function createCustomRecord($parameters)
    {
        $parameters['currency']  = Currency::all();
        return $parameters;
    }
   public function store()
    {
        Plan::create([
            'name_033'          => $this->request->input('name_033'),
            'price_033'       => $this->request->input('price_033'),
            'alph_code_currency_033'         => $this->request->input('alph_code_currency_033'),
            'description_033'       => $this->request->input('description_033')
            
        ]);
       return redirect()->route($this->routeSuffix)->with([
            'msg'        => 1,
            'txtMsg'     => trans('sergeant::sergeant.message_create_record_successful', [
            'name' => $this->request->input('name_033')
            ])
        ]); 
    }
    public function editCustomRecord($parameters)
    {
        $parameters['currency']  = Currency::all();
        return $parameters;
    }
    public function update($parameters)
    {
        $plan = [
            'name_033'          => $this->request->input('name_033'),
            'price_033'       => $this->request->input('price_033'),
            'alph_code_currency_033'         => $this->request->input('alph_code_currency_033'),
            'description_033'       => $this->request->input('description_033'),
        ];
        
        Plan::where('id_033',$parameters)->update($plan);

        return redirect()->route($this->routeSuffix)->with([
            'msg'        => 1,
            'txtMsg'     => trans('sergeant::sergeant.message_update_record', [
            'name' => $this->request->input('name_033')
            ])
        ]); 
    }
}