<?php namespace Rent\Sergeant\Controllers;

use Illuminate\Support\Facades\DB;
use Rent\Sergeant\Core\Controller;

// uses to testing
use Maatwebsite\Excel\Facades\Excel;
use Rent\Sergeant\Models\ReportTask;


/**
 * Class TestingController
 * @package Rent\Sergeant\Controllers
 */

class TestingController extends Controller
{
    public function testing()
    {

        $reportTasks = ReportTask::builder()
            ->get();

        foreach ($reportTasks as $reportTask)
        {
            // Execute query from report task
            $response = DB::select(DB::raw($reportTask->sql_023));

            // if has results from query
            if(count($response) === 0)
                dd('no hay resultados');

            // format response to manage with collections
            $response = collect(array_map(function($item) {
                return $item;
            }, $response));

            dd($response);
        }



    }
}