<?php namespace Rent\Sergeant\Controllers;

use Rent\Sergeant\Core\Controller;
use Illuminate\Support\Facades\Hash;
use Rent\Sergeant\Models\Lang;
use Rent\Sergeant\Models\Profile;
use Rent\Sergeant\Models\User;
use Rent\Sergeant\Models\Plaza;
use Rent\Sergeant\Models\Plan;
use DB;
use Input;

/**
 * Class UserController
 * @package Rent\Sergeant\Controllers
 */

class UserController extends Controller
{
    protected $routeSuffix  = 'user';
    protected $folder       = 'user';
    protected $package      = 'sergeant';
    protected $indexColumns = ['id_010', 'name_010', 'surname_010', ['data' => 'email_010', 'type' => 'email'], 'name_006', ['data' => 'access_010', 'type' => 'active']];
    protected $nameM        = 'name_010';
    protected $model        = User::class;
    protected $icon         = 'fa fa-users';
    protected $objectTrans  = 'user';

    public function createCustomRecord($parameters)
    {
        $user = New User();
        $parameters['langs']    = Lang::getActivesLangs();
        $parameters['profiles'] = Profile::all();
        $parameters['plans'] = Plan::all();
        if($user->getActivePlan() != null){
            $parameters['active_plan'] = $user->getActivePlan();
        }
        return $parameters;
    }
    
    public function storeCustomRecord($parameters)
    {
        $user = User::create([
                    'name_010'          => $this->request->input('name'),
                    'surname_010'       => $this->request->input('surname'),
                    'email_010'         => $this->request->input('email'),
                    'lang_id_010'       => $this->request->input('lang'),
                    'access_010'        => $this->request->input('access',0),
                    'profile_id_010'    => $this->request->input('profile'),
                    'user_010'          => $this->request->input('user'),
                    'password_010'      => Hash::make($this->request->input('password'))
                ]);
        if($user->profile_id_010 != 1){
            DB::table('001_031_plan_user')->insert([
                ['id_010' => $user->id_010, 'id_033' => Input::get('plan'), 'active_031' => 1]
            ]);        
        }
    }

    public function editCustomRecord($parameters)
    {
        $user = New User();
        $parameters['langs']    = Lang::getActivesLangs();
        $parameters['profiles'] = Profile::all();
        $parameters['plans'] = Plan::all();
        if($user->getActivePlan() != null){
        $parameters['active_plan'] = $user->getActivePlan();
        }
        return $parameters;
    }

    public function checkSpecialRulesToUpdate($parameters)
    {
        $user = User::find($parameters['id']);

        $parameters['specialRules']['emailRule']    = $this->request->input('email') == $user->email_010? true : false;
        $parameters['specialRules']['userRule']     = $this->request->input('user') == $user->user_010? true : false;
        $parameters['specialRules']['passRule']     = ! $this->request->has('password');

        return $parameters;
    }

    public function updateCustomRecord($parameters)
    {
        $user = [
            'name_010'          => $this->request->input('name'),
            'surname_010'       => $this->request->input('surname'),
            'email_010'         => $this->request->input('email'),
            'lang_id_010'       => $this->request->input('lang'),
            'access_010'        => $this->request->input('access',0),
            'profile_id_010'    => $this->request->input('profile'),
            'user_010'          => $this->request->input('user'),
        ];

        if($parameters['specialRules']['emailRule'])  $user['email_010']      = $this->request->input('email');
        if($parameters['specialRules']['userRule'])   $user['user_010']       = $this->request->input('user');
        if(! $parameters['specialRules']['passRule']) $user['password_010']   = Hash::make($this->request->input('password'));

        User::where('id_010', $parameters['id'])->update($user);
        
        $user_has_plan = \Rent\Sergeant\Models\PlanUser::where('id_010', '=', $parameters['id'])->where('id_033', '=', $this->request->input('plan'))->first();
        
        if($this->request->input('profile') != 1 && $user_has_plan === null){
            DB::table('001_031_plan_user')->insert([
                ['id_010' => $user->id_010, 'id_033' => Input::get('plan'), 'active_031' => 1]
            ]);        
        }elseif ($this->request->input('profile') != 1 && $user_has_plan !== null) {
             DB::table('001_031_plan_user')
            ->where('id_010',  $user->id_010)
            ->update(['active_031' => 0]);
             
            DB::table('001_031_plan_user')
            ->where('id_010',  $user->id_010)
            ->where('id_033',  Input::get('plan'))
            ->update(['active_031' => 1]);
        }
        
    }
}