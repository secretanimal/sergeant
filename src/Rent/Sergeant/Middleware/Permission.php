<?php namespace Rent\Sergeant\Middleware;

use Closure;

class Permission
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// check permission user, all parameters ['resource', 'action'] are passed in route.php file
		$action = $request->route()->getAction();

		if(isset($action['resource']))
			if(!is_allowed($action['resource'], $action['action']))
				return view('sergeant::errors.default', [
					'error'     => 403,
					'message'   => trans('sergeant::sergeant.message_error_403')
				]);

		return $next($request);
	}
}