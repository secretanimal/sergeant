<?php namespace Rent\Sergeant\Models;

use Rent\Sergeant\Core\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Support\Facades\Validator;

/**
 * Class Package
 *
 * Model with properties
 * <br><b>[id, name, folder, active, sorting]</b>
 *
 * @package     Rent\Sergeant\Models
 */

class Currency extends Model
{
    use Eloquence, Mappable;

    protected $table        = '001_030_currency';
    protected $primaryKey   = 'id_030';
    protected $suffix        = '030';
    public $timestamps      = false;
    protected $fillable     = ['id_030', 'alphabetic_code_030', 'numeric_code_030', 'name_030', 'simbol_030'];
    protected $maps         = [];
    protected $relationMaps = [];
    private static $rules   = [
        'alphabetic_code'    =>  'required|between:3',
        'name' =>  'required|between:2,30'
    ];
        
    public static function validate($data, $specialRules = [])
    {
        return Validator::make($data, static::$rules);
	}

    public function scopeBuilder($query)
    {
        return $query;
    }

    public static function getRecords($args)
    {
        $query = Package::query();

        if(isset($args['aplphabetic_code_030']))    $query->where('alphabetic_code_030', $args['alphabetic_code_030']);
        if(isset($args['orderBy']))     $query->orderBy($args['orderBy']['column'], $args['orderBy']['order']);

        return $query->get();
    }
    
}