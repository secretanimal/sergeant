<?php namespace Rent\Sergeant\Models;

use Rent\Sergeant\Core\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Support\Facades\Validator;

/**
 * Class Package
 *
 * Model with properties
 * <br><b>[id, name, folder, active, sorting]</b>
 *
 * @package     Rent\Sergeant\Models
 */

class Plan extends Model
{
    use Eloquence, Mappable;

    protected $table        = '001_033_plan';
    protected $primaryKey   = 'id_033';
    protected $suffix        = '033';
    public $timestamps      = false;
    protected $fillable     = ['id_033', 'name_033', 'price_033', 'alph_code_currency_033', 'description_033'];
    protected $maps         = [];
    protected $relationMaps = [
        'currency'   => Currency::class,];
    private static $rules   = [
        'name'    =>  'required|between:2,50',
        'price' =>  'required',
        'alph_code_currency' => 'required'
    ];
        
    public static function validate($data, $specialRules = [])
    {
        return Validator::make($data, static::$rules);
	}

    public function scopeBuilder($query)
    {
        return $query->join('001_030_currency', '001_033_plan.alph_code_currency_033', '=', '001_030_currency.alphabetic_code_030');
    }

    public static function getRecords($args)
    {
        $query = Package::query();

        if(isset($args['name_033']))    $query->where('name_033', $args['name_033']);
        if(isset($args['orderBy']))     $query->orderBy($args['orderBy']['column'], $args['orderBy']['order']);

        return $query->get();
    }
    /**
     * Get user from plaza
     *
     * @return \Rent\Sergeant\Models\User
     */
    public function getCurrency()
    {
        return $this->belongsTo(Currency::class, 'alphabetic_code_030');
    }
    
    public function getPlan()
    {
         $plan = Plan::all();           
          return (array)$plan;
    }
    /*Get plans for User*/
    public function getPlanUser(){
        $plans = DB::table('001_033_plan')
            ->leftJoin('001_031_plan_user', '001_033_plan.id_033', '=', '001_031_plan_user.id_033')
            ->where('001_033_plan', '=', auth('sergeant')->user()->id_010)
            ->get();
    }
}