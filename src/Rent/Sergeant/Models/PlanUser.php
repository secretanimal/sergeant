<?php namespace Rent\Sergeant\Models;

use Rent\Sergeant\Core\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Support\Facades\Validator;
use DB;

/**
 * Class Package
 *
 * Model with properties
 * <br><b>[id, name, folder, active, sorting]</b>
 *
 * @package     Rent\Sergeant\Models
 */

class PlanUser extends Model
{
    use Eloquence, Mappable;

    protected $table        = '001_031_plan_user';
    protected $primaryKey   = 'id_031';
    protected $suffix        = '031';
    public $timestamps      = false;
    protected $fillable     = ['id_010', 'id_033', 'active_031'];
    protected $maps         = [];
    protected $relationMaps = [
        'user'  => User::class, 
        'plan'  => Plan::class,
    ];
    
    /**
     * Get Plaza from PlanUser
     *
     * @return \Rent\Sergeant\Models\Plan
     */
    public function getPlan()
    {
        return $this->belongsTo(Plan::class, 'id_033');
    }
    
    /**
     * Get User from PlanUser
     *
     * @return \Rent\Sergeant\Models\User
     */
    public function getUser()
    {
        return $this->belongsTo(User::class, 'id_010');
    }
    
    
}