<?php namespace Rent\Sergeant\Models;

use Rent\Sergeant\Core\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Support\Facades\Validator;

/**
 * Class Package
 *
 * Model with properties
 * <br><b>[id, name, folder, active, sorting]</b>
 *
 * @package     Rent\Sergeant\Models
 */

class Plaza extends Model
{
    use Eloquence, Mappable;

    protected $table        = '001_029_plaza';
    protected $primaryKey   = 'id_029';
    protected $suffix        = '029';
    public $timestamps      = false;
    protected $fillable     = ['id_029', 'name_029', 'address_029', 'user_id_029', 'plano_029'];
    protected $maps         = [];
    protected $relationMaps = [
        'user'   => User::class,];
    private static $rules   = [
        'name'    =>  'required|between:2,50',
        'user_id' =>  'required'
    ];
        
    public static function validate($data, $specialRules = [])
    {
        return Validator::make($data, static::$rules);
	}

    public function scopeBuilder($query)
    {
        return $query->join('001_010_user', '001_029_plaza.user_id_029', '=', '001_010_user.id_010');
    }

    public static function getRecords($args)
    {
        $query = Package::query();

        if(isset($args['name_029']))    $query->where('name_029', $args['name_029']);
        if(isset($args['orderBy']))     $query->orderBy($args['orderBy']['column'], $args['orderBy']['order']);

        return $query->get();
    }
    /**
     * Get user from plaza
     *
     * @return \Rent\Sergeant\Models\User
     */
    public function getUser()
    {
        return $this->belongsTo(User::class, 'id_010');
    }
}