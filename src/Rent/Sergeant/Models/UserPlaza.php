<?php namespace Rent\Sergeant\Models;

use Rent\Sergeant\Core\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Support\Facades\Validator;

/**
 * Class Package
 *
 * Model with properties
 * <br><b>[id, name, folder, active, sorting]</b>
 *
 * @package     Rent\Sergeant\Models
 */

class UserPlaza extends Model
{
    use Eloquence, Mappable;

    protected $table        = '001_036_plaza_users_access';
    protected $primaryKey   = 'id_036';
    protected $suffix        = '036';
    public $timestamps      = false;
    protected $fillable     = ['id_010', 'id_029'];
    protected $maps         = [];
    protected $relationMaps = [
        'user'   => User::class, 
        'plaza'  => Plaza::class,
    ];
    
    /**
     * Get Plaza from UserPlaza
     *
     * @return \Rent\Sergeant\Models\Plaza
     */
    public function getPlaza()
    {
        return $this->belongsTo(Plaza::class, 'id_029');
    }
    
    /**
     * Get User from UserPlaza
     *
     * @return \Rent\Sergeant\Models\User
     */
    public function getUser()
    {
        return $this->belongsTo(User::class, 'id_010');
    }
}