<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SergeantCreateTableCurrency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('001_030_currency'))
		{
			Schema::create('001_030_currency', function (Blueprint $table) {
				$table->engine = 'InnoDB';
				
				$table->increments('id_030')->unsigned();
                                $table->string('name_030');
				$table->string('alphabetic_code_030',3)->unique();
                                $table->string('numeric_code_030',3)->unique();
                                $table->string('simbol_030')->nullable();
                                
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('001_030_currency'))
        {
            Schema::drop('001_030_currency');
        }
    }
}
