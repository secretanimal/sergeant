<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SergeantCreateTablePlanUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('001_031_plan_user'))
        {
            Schema::create('001_031_plan_user', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                
                $table->integer('id_010')->unsigned();
                $table->integer('id_033')->unsigned();
               $table->primary(['id_010', 'id_033']);
                $table->foreign('id_010', 'fk01_001_031_plan_user')
                    ->references('id_010')
                    ->on('001_010_user')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
                $table->foreign('id_033', 'fk02_001_031_plan_user')
                    ->references('id_033')
                    ->on('001_033_plan')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
