<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SergeantCreateTablePlazaUsersAccess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('001_036_plaza_users_access'))
        {
            Schema::create('001_036_plaza_users_access', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                
                $table->integer('id_010')->unsigned();
                $table->integer('id_029')->unsigned();
               $table->primary(['id_010', 'id_029']);
                $table->foreign('id_010', 'fk01_001_036_plaza_users_access')
                    ->references('id_010')
                    ->on('001_010_user')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
                $table->foreign('id_029', 'fk02_001_036_plaza_users_access')
                    ->references('id_029')
                    ->on('001_029_plaza')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
