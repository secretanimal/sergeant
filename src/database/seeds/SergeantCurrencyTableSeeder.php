<?php

use Illuminate\Database\Seeder;
use Rent\Sergeant\Models\Currency;

class SergeantCurrencyTableSeeder extends Seeder
{
    public function run()
    {
        Currency::insert([
            ['id_030' => 1,  'alphabetic_code_030' => 'USD',     'numeric_code_030' => '840',    'name_030' => 'Dólar Estadounidense',   'simbol_030' => '$'],
            ['id_030' => 2,  'alphabetic_code_030' => 'MXN',     'numeric_code_030' => '484',    'name_030' => 'Peso Mexicano',   'simbol_030' => '$']
        ]);
    }
}

/*
 * Comand to run:
 * php artisan db:seed --class="SergeantCurrencyTableSeeder"
 */