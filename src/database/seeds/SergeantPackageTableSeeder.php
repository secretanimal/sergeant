<?php

use Illuminate\Database\Seeder;
use Rent\Sergeant\Models\Package;

class SergeantPackageTableSeeder extends Seeder
{
    public function run()
    {
        Package::insert([
            ['id_012' => '1',   'name_012' => 'Sergeant',                         'folder_012' => '',             'sorting_012' => 1,     'active_012' => '1'],
            ['id_012' => '2',   'name_012' => 'Sergeant Administration Package',  'folder_012' => 'sergeant',       'sorting_012' => 2,     'active_012' => '1'],
        ]);
    }
}

/*
 * Command to run:
 * php artisan db:seed --class="SergeantPackageTableSeeder"
 */