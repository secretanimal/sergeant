<?php

use Illuminate\Database\Seeder;
use Rent\Sergeant\Models\Plan;

class SergeantPlanTableSeeder extends Seeder
{
   
    public function run()
    {
        Plan::insert([
            ['id_033' => 1,  'name_033' => 'TRY OUT',     'price_033' => 0.0,    'alph_code_currency_033' => 'MXN',   'description_033' => 'Plan Gratis: para 5 locales'],
            ['id_033' => 2,  'name_033' => 'BÁSICO',     'price_033' => 120.00,    'alph_code_currency_033' => 'MXN',   'description_033' => 'Plan Básico: Incluye # de locales ilimitados'],
        
            ['id_033' => 3,  'name_033' => 'PRO',     'price_033' => 170.00,  'alph_code_currency_033' => 'MXN',   'description_033' => 'Plan PRO: Incluye # de locales ilimitados y Módulo de Ventas'],
            ['id_033' => 4,  'name_033' => 'PREMIUM',     'price_033' => 200.00,  'alph_code_currency_033' => 'MXN',   'description_033' => 'Plan PREMIUM: Incluye # de locales ilimitados y todos los módulos']
            ]);
    }
}

/*
 * Comand to run:
 * php artisan db:seed --class="SergeantPlanTableSeeder"
 */