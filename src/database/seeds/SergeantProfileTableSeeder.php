<?php

use Illuminate\Database\Seeder;
use Rent\Sergeant\Models\Profile;

class SergeantProfileTableSeeder extends Seeder
{
    public function run()
    {
        Profile::insert([
            ['id_006' => '1','name_006' => 'Administrador'],
            ['id_006' => '2','name_006' => 'Ejecutivo']
        ]);
    }
}

/*
 * Command to run:
 * php artisan db:seed --class="SergeantProfileTableSeeder"
 */