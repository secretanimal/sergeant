<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SergeantTableSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        $this->call(SergeantLangTableSeeder::class);
        $this->call(SergeantCountryTableSeeder::class);
        $this->call(SergeantTerritorialArea1TableSeeder::class);
        $this->call(SergeantTerritorialArea2TableSeeder::class);
        $this->call(SergeantActionTableSeeder::class);
        $this->call(SergeantPackageTableSeeder::class);
        $this->call(SergeantProfileTableSeeder::class);
        $this->call(SergeantResourceTableSeeder::class);
        $this->call(SergeantUserTableSeeder::class);
        $this->call(SergeantPermissionTableSeeder::class);
        $this->call(SergeantCurrencyTableSeeder::class);
        $this->call(SergeantPlanTableSeeder::class);
        Model::reguard();
    }
}

/*
 * Command to run:
 * php artisan db:seed --class="SergeantTableSeeder"
 */