<?php

use Illuminate\Database\Seeder;
use Rent\Sergeant\Models\User;

class SergeantUserTableSeeder extends Seeder
{
    public function run()
    {
        User::insert([
            ['id_010' => '1','remember_token_010' => NULL,'lang_id_010' => 'en','profile_id_010' => '1','access_010' => '1','user_010' => 'admin@sergeant.local','password_010' => '$2y$10$3eFZAd31wPmg2mMZB/CZ4.CkcZKY9xr7A3Z9ou6mp7OkSIc3Qo.yW','email_010' => 'adminn@sergeant.local','name_010' => 'Sergeant','surname_010' => 'Sergeant','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00']
        ]);
    }
}

/*
 * Comand to run:
 * php artisan db:seed --class="SergeantUserTableSeeder"
 */