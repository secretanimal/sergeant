<?php

Route::group(['middleware' => ['web']], function() {

    // Enter to application
    Route::get(config('sergeant.appName'), function () {
        return redirect()->route('dashboard');
    });

    /*
    |--------------------------------------------------------------------------
    | DASHBOARD
    |--------------------------------------------------------------------------
    */
    Route::get(config('sergeant.appName') . '/sergeant/dashboard',      ['middleware' => 'sergeant.auth',         'as' => 'dashboard', 'uses' => 'Rent\Sergeant\Controllers\DashboardController@index',              'resource' => 'admin-dashboard']);

    /*
    |--------------------------------------------------------------------------
    | LOGIN AND LOGOUT
    |--------------------------------------------------------------------------
    */
    Route::get(config('sergeant.appName') . '/sergeant/login',                              ['as' => 'sergeantGetLogin',              'uses' => 'Rent\Sergeant\Controllers\Auth\AuthController@getLogin']);
    Route::post(config('sergeant.appName') . '/sergeant/login',                             ['as' => 'sergeantPostLogin',             'uses' => 'Rent\Sergeant\Controllers\Auth\AuthController@authenticate']);
    Route::get(config('sergeant.appName') . '/sergeant/logout',                             ['as' => 'sergeantLogout',                'uses' => 'Rent\Sergeant\Controllers\Auth\AuthController@getLogout']);

    /*
    |--------------------------------------------------------------------------
    | REGISTER
    |--------------------------------------------------------------------------
    */
    Route::get(config('sergeant.appName') . '/sergeant/register',                           ['as' => 'sergeantGetRegister',              'uses' => 'Rent\Sergeant\Controllers\Auth\AuthController@getRegister']);
    Route::post(config('sergeant.appName') . '/sergeant/register',                          ['as' => 'sergeantPostRegister',             'uses' => 'Rent\Sergeant\Controllers\Auth\AuthController@store']);
    Route::get(config('sergeant.appName') . '/sergeant/register/verify/{confirmationCode}', ['as' => 'sergeantConfirmation', 'uses' => 'Rent\Sergeant\Controllers\Auth\AuthController@confirm']);
    Route::get(config('sergeant.appName') . '/sergeant/get_reset', ['as' => 'sergeantGetReset', 'uses' => 'Rent\Sergeant\Controllers\Auth\AuthController@getReset']);
    /*
    |--------------------------------------------------------------------------
    | PASSWORD REMINDER
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/email/send/reset/link/email',        ['as' => 'sendResetLinkEmail',          'uses' => 'Rent\Sergeant\Controllers\Auth\PasswordController@postEmail']);
    Route::get(config('sergeant.appName') . '/sergeant/password/reset/{token}',             ['as' => 'showResetForm',               'uses' => 'Rent\Sergeant\Controllers\Auth\PasswordController@getReset']);
    Route::post(config('sergeant.appName') . '/sergeant/password/reset/{token}',            ['as' => 'postResetPassword',           'uses' => 'Rent\Sergeant\Controllers\Auth\PasswordController@postReset']);

    /*
    |--------------------------------------------------------------------------
    | DOWNLOAD ADVANCED SEARCH
    |--------------------------------------------------------------------------
    */
    Route::get(config('sergeant.appName') . '/sergeant/advanced/search/download/{token}',   ['as' => 'downloadAdvancedSearch',      'uses' => 'Rent\Sergeant\Controllers\DownloadController@downloadAdvancedSearch']);

    /*
    |--------------------------------------------------------------------------
    | COUNTRIES
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/countries/json/countries/{lang?}',                               ['as' => 'jsonGetCountries',          'uses' => 'Rent\Sergeant\Controllers\CountryController@jsonCountries', 'resource' => 'admin-country', 'action' => 'access']);

    /*
    |--------------------------------------------------------------------------
    | TERRITORIAL AREAS 1
    |--------------------------------------------------------------------------
    */
    Route::post(config('sergeant.appName') . '/sergeant/territorialareas1/json/from/country/{country?}',                ['as' => 'jsonGetTerritorialArea1',   'uses' => 'Rent\Sergeant\Controllers\TerritorialArea1Controller@jsonTerritorialAreas1FromCountry', 'resource' => 'admin-country-at1', 'action' => 'access']);

    /*
    |--------------------------------------------------------------------------
    | TERRITORIAL AREAS 2
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/territorialareas2/json/from/territorialarea1/{country}/{id}',    ['as' => 'jsonTerritorialArea2',      'uses' => 'Rent\Sergeant\Controllers\TerritorialArea2Controller@jsonTerritorialAreas2FromTerritorialArea1', 'resource' => 'admin-country-at2', 'action' => 'access']);

    /*
    |--------------------------------------------------------------------------
    | TERRITORIAL AREAS 3
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/territorialareas3/json/from/territorialarea2/{country}/{id?}',   ['as' => 'jsonTerritorialArea3',      'uses' => 'Rent\Sergeant\Controllers\TerritorialArea3Controller@jsonTerritorialAreas3FromTerritorialArea2', 'resource' => 'admin-country-at3', 'action' => 'access']);
});

Route::group(['middleware' => ['web', 'sergeant']], function() {

    /*
    |--------------------------------------------------------------------------
    | TESTING CONTROLLER
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/testing',                                    ['as' => 'testing',                'uses' => 'Rent\Sergeant\Controllers\TestingController@testing']);

    /*
    |--------------------------------------------------------------------------
    | ACTIONS
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/actions/{offset?}',                          ['as' => 'action',                'uses' => 'Rent\Sergeant\Controllers\ActionController@index',                      'resource' => 'admin-perm-action',        'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/actions/json/data',                          ['as' => 'jsonDataAction',        'uses' => 'Rent\Sergeant\Controllers\ActionController@jsonData',                   'resource' => 'admin-perm-action',        'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/actions/create/{offset}',                    ['as' => 'createAction',          'uses' => 'Rent\Sergeant\Controllers\ActionController@createRecord',               'resource' => 'admin-perm-action',        'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/actions/store/{offset}',                    ['as' => 'storeAction',           'uses' => 'Rent\Sergeant\Controllers\ActionController@storeRecord',                'resource' => 'admin-perm-action',        'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/actions/{id}/edit/{offset}',                 ['as' => 'editAction',            'uses' => 'Rent\Sergeant\Controllers\ActionController@editRecord',                 'resource' => 'admin-perm-action',        'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/actions/update/{id}/{offset}',               ['as' => 'updateAction',          'uses' => 'Rent\Sergeant\Controllers\ActionController@updateRecord',               'resource' => 'admin-perm-action',        'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/actions/delete/{id}/{offset}',               ['as' => 'deleteAction',          'uses' => 'Rent\Sergeant\Controllers\ActionController@deleteRecord',               'resource' => 'admin-perm-action',        'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/actions/delete/select/records',           ['as' => 'deleteSelectAction',    'uses' => 'Rent\Sergeant\Controllers\ActionController@deleteRecordsSelect',        'resource' => 'admin-perm-action',        'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | RESOURCES
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/resources/{offset?}',                        ['as' => 'resource',              'uses' => 'Rent\Sergeant\Controllers\ResourceController@index',                    'resource' => 'admin-perm-resource',    'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/resources/json/data',                        ['as' => 'jsonDataResource',      'uses' => 'Rent\Sergeant\Controllers\ResourceController@jsonData',                 'resource' => 'admin-perm-resource',    'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/resources/create/{offset}',                  ['as' => 'createResource',        'uses' => 'Rent\Sergeant\Controllers\ResourceController@createRecord',             'resource' => 'admin-perm-resource',    'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/resources/store/{offset}',                  ['as' => 'storeResource',         'uses' => 'Rent\Sergeant\Controllers\ResourceController@storeRecord',              'resource' => 'admin-perm-resource',    'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/resources/{id}/edit/{offset}',               ['as' => 'editResource',          'uses' => 'Rent\Sergeant\Controllers\ResourceController@editRecord',               'resource' => 'admin-perm-resource',    'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/resources/update/{id}/{offset}',             ['as' => 'updateResource',        'uses' => 'Rent\Sergeant\Controllers\ResourceController@updateRecord',             'resource' => 'admin-perm-resource',    'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/resources/delete/{id}/{offset}',             ['as' => 'deleteResource',        'uses' => 'Rent\Sergeant\Controllers\ResourceController@deleteRecord',             'resource' => 'admin-perm-resource',    'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/resources/delete/select/records',         ['as' => 'deleteSelectResource',  'uses' => 'Rent\Sergeant\Controllers\ResourceController@deleteRecordsSelect',      'resource' => 'admin-perm-resource',    'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | PROFILES
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/profiles/{offset?}',                         ['as' => 'profile',               'uses' => 'Rent\Sergeant\Controllers\ProfileController@index',                     'resource' => 'admin-perm-profile',    'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/profiles/json/data',                         ['as' => 'jsonDataProfile',       'uses' => 'Rent\Sergeant\Controllers\ProfileController@jsonData',                  'resource' => 'admin-perm-profile',    'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/profiles/create/{offset}',                   ['as' => 'createProfile',         'uses' => 'Rent\Sergeant\Controllers\ProfileController@createRecord',              'resource' => 'admin-perm-profile',    'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/profiles/store/{offset}',                   ['as' => 'storeProfile',          'uses' => 'Rent\Sergeant\Controllers\ProfileController@storeRecord',               'resource' => 'admin-perm-profile',    'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/profiles/{id}/edit/{offset}',                ['as' => 'editProfile',           'uses' => 'Rent\Sergeant\Controllers\ProfileController@editRecord',                'resource' => 'admin-perm-profile',    'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/profiles/update/{id}/{offset}',              ['as' => 'updateProfile',         'uses' => 'Rent\Sergeant\Controllers\ProfileController@updateRecord',              'resource' => 'admin-perm-profile',    'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/profiles/delete/{id}/{offset}',              ['as' => 'deleteProfile',         'uses' => 'Rent\Sergeant\Controllers\ProfileController@deleteRecord',              'resource' => 'admin-perm-profile',    'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/profiles/delete/select/records',          ['as' => 'deleteSelectProfile',   'uses' => 'Rent\Sergeant\Controllers\ProfileController@deleteRecordsSelect',       'resource' => 'admin-perm-profile',    'action' => 'delete']);
    Route::get(config('sergeant.appName') . '/sergeant/profiles/allpermissions/{id}/{offset}',      ['as' => 'allPermissionsProfile', 'uses' => 'Rent\Sergeant\Controllers\ProfileController@setAllPermissions',         'resource' => 'admin-perm-perm',       'action' => 'create']);

    /*
    |--------------------------------------------------------------------------
    | PERMISSIONS
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/permissions/{offset}/{profile}/{offsetProfile?}',        ['as' => 'permission',                'uses' => 'Rent\Sergeant\Controllers\PermissionController@index',                'resource' => 'admin-perm-perm',    'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/permissions/json/data/profile/{profile}',                ['as' => 'jsonDataPermission',        'uses' => 'Rent\Sergeant\Controllers\PermissionController@jsonData',             'resource' => 'admin-perm-perm',    'action' => 'access']);
    Route::post(config('sergeant.appName') . '/sergeant/permissions/json/create/{profile}/{resource}/{action}', ['as' => 'jsonCreatePermission',      'uses' => 'Rent\Sergeant\Controllers\PermissionController@jsonCreate',           'resource' => 'admin-perm-perm',    'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/permissions/json/delete/{profile}/{resource}/{action}', ['as' => 'jsonDestroyPermission',     'uses' => 'Rent\Sergeant\Controllers\PermissionController@jsonDestroy',          'resource' => 'admin-perm-perm',    'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | PACKAGES
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/packages/{offset?}',                         ['as' => 'package',               'uses' => 'Rent\Sergeant\Controllers\PackageController@index',                     'resource' => 'admin-package',         'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/packages/json/data',                         ['as' => 'jsonDataPackage',       'uses' => 'Rent\Sergeant\Controllers\PackageController@jsonData',                  'resource' => 'admin-package',         'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/packages/create/{offset}',                   ['as' => 'createPackage',         'uses' => 'Rent\Sergeant\Controllers\PackageController@createRecord',              'resource' => 'admin-package',         'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/packages/store/{offset}',                   ['as' => 'storePackage',          'uses' => 'Rent\Sergeant\Controllers\PackageController@storeRecord',               'resource' => 'admin-package',         'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/packages/{id}/edit/{offset}',                ['as' => 'editPackage',           'uses' => 'Rent\Sergeant\Controllers\PackageController@editRecord',                'resource' => 'admin-package',         'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/packages/update/{id}/{offset}',              ['as' => 'updatePackage',         'uses' => 'Rent\Sergeant\Controllers\PackageController@updateRecord',              'resource' => 'admin-package',         'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/packages/delete/{id}/{offset}',              ['as' => 'deletePackage',         'uses' => 'Rent\Sergeant\Controllers\PackageController@deleteRecord',              'resource' => 'admin-package',         'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/packages/delete/select/records',          ['as' => 'deleteSelectPackage',   'uses' => 'Rent\Sergeant\Controllers\PackageController@deleteRecordsSelect',       'resource' => 'admin-package',         'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | CRON JOBS
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/cronjobs/{offset?}',                         ['as' => 'cronJob',               'uses' => 'Rent\Sergeant\Controllers\CronJobController@index',                     'resource' => 'admin-cron',             'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/cronjobs/{id}/run/{offset}',                 ['as' => 'runCronJob',            'uses' => 'Rent\Sergeant\Controllers\CronJobController@run',                       'resource' => 'admin-cron',             'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/cronjobs/json/data',                         ['as' => 'jsonDataCronJob',       'uses' => 'Rent\Sergeant\Controllers\CronJobController@jsonData',                  'resource' => 'admin-cron',             'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/cronjobs/create/{offset}',                   ['as' => 'createCronJob',         'uses' => 'Rent\Sergeant\Controllers\CronJobController@createRecord',              'resource' => 'admin-cron',             'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/cronjobs/store/{offset}',                   ['as' => 'storeCronJob',          'uses' => 'Rent\Sergeant\Controllers\CronJobController@storeRecord',               'resource' => 'admin-cron',             'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/cronjobs/{id}/edit/{offset}',                ['as' => 'editCronJob',           'uses' => 'Rent\Sergeant\Controllers\CronJobController@editRecord',                'resource' => 'admin-cron',             'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/cronjobs/update/{id}/{offset}',              ['as' => 'updateCronJob',         'uses' => 'Rent\Sergeant\Controllers\CronJobController@updateRecord',              'resource' => 'admin-cron',             'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/cronjobs/delete/{id}/{offset}',              ['as' => 'deleteCronJob',         'uses' => 'Rent\Sergeant\Controllers\CronJobController@deleteRecord',              'resource' => 'admin-cron',             'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/cronjobs/delete/select/records',          ['as' => 'deleteSelectCronJob',   'uses' => 'Rent\Sergeant\Controllers\CronJobController@deleteRecordsSelect',       'resource' => 'admin-cron',             'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | REPORTS
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/reports/{offset?}',                          ['as' => 'reportTask',              'uses' => 'Rent\Sergeant\Controllers\ReportTaskController@index',                'resource' => 'admin-report',             'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/reports/{id}/run/{offset}',                  ['as' => 'runReportTask',           'uses' => 'Rent\Sergeant\Controllers\ReportTaskController@run',                  'resource' => 'admin-report',             'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/reports/json/data',                          ['as' => 'jsonDataReportTask',      'uses' => 'Rent\Sergeant\Controllers\ReportTaskController@jsonData',             'resource' => 'admin-report',             'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/reports/create/{offset}',                    ['as' => 'createReportTask',        'uses' => 'Rent\Sergeant\Controllers\ReportTaskController@createRecord',         'resource' => 'admin-report',             'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/reports/store/{offset}',                    ['as' => 'storeReportTask',         'uses' => 'Rent\Sergeant\Controllers\ReportTaskController@storeRecord',          'resource' => 'admin-report',             'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/reports/{id}/edit/{offset}',                 ['as' => 'editReportTask',          'uses' => 'Rent\Sergeant\Controllers\ReportTaskController@editRecord',           'resource' => 'admin-report',             'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/reports/update/{id}/{offset}',               ['as' => 'updateReportTask',        'uses' => 'Rent\Sergeant\Controllers\ReportTaskController@updateRecord',         'resource' => 'admin-report',             'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/reports/delete/{id}/{offset}',               ['as' => 'deleteReportTask',        'uses' => 'Rent\Sergeant\Controllers\ReportTaskController@deleteRecord',         'resource' => 'admin-report',             'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/reports/delete/select/records',           ['as' => 'deleteSelectReportTask',  'uses' => 'Rent\Sergeant\Controllers\ReportTaskController@deleteRecordsSelect',  'resource' => 'admin-report',             'action' => 'delete']);
    Route::get(config('sergeant.appName') . '/sergeant/reports/download/{filename}/{token}',        ['as' => 'downloadReportTask',      'uses' => 'Rent\Sergeant\Controllers\DownloadController@downloadAReportTask']);

    /*
    |--------------------------------------------------------------------------
    | LANGUAGES
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/langs/{offset?}',                            ['as' => 'lang',                'uses' => 'Rent\Sergeant\Controllers\LangController@index',                      'resource' => 'admin-lang',             'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/langs/json/data',                            ['as' => 'jsonDataLang',        'uses' => 'Rent\Sergeant\Controllers\LangController@jsonData',                   'resource' => 'admin-lang',             'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/langs/create/{offset}',                      ['as' => 'createLang',          'uses' => 'Rent\Sergeant\Controllers\LangController@createRecord',               'resource' => 'admin-lang',             'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/langs/store/{offset}',                      ['as' => 'storeLang',           'uses' => 'Rent\Sergeant\Controllers\LangController@storeRecord',                'resource' => 'admin-lang',             'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/langs/{id}/edit/{offset}',                   ['as' => 'editLang',            'uses' => 'Rent\Sergeant\Controllers\LangController@editRecord',                 'resource' => 'admin-lang',             'action' => 'create']);
    Route::put(config('sergeant.appName') . '/sergeant/langs/update/{id}/{offset}',                 ['as' => 'updateLang',          'uses' => 'Rent\Sergeant\Controllers\LangController@updateRecord',               'resource' => 'admin-lang',             'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/langs/delete/{id}/{offset}',                 ['as' => 'deleteLang',          'uses' => 'Rent\Sergeant\Controllers\LangController@deleteRecord',               'resource' => 'admin-lang',             'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/langs/delete/select/records',             ['as' => 'deleteSelectLang',    'uses' => 'Rent\Sergeant\Controllers\LangController@deleteRecordsSelect',        'resource' => 'admin-lang',             'action' => 'delete']);
    Route::post(config('sergeant.appName') . '/sergeant/langs/delete/image/lang/{id}',              ['as' => 'deleteImageLang',     'uses' => 'Rent\Sergeant\Controllers\LangController@ajaxDeleteImage',            'resource' => 'admin-lang',             'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | COUNTRIES
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/countries/{lang}/{offset?}',                         ['as' => 'country',                   'uses' => 'Rent\Sergeant\Controllers\CountryController@index',                    'resource' => 'admin-country',          'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/countries/json/data/{lang}',                         ['as' => 'jsonDataCountry',           'uses' => 'Rent\Sergeant\Controllers\CountryController@jsonData',                 'resource' => 'admin-country',          'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/countries/create/{lang}/{offset}/{id?}',             ['as' => 'createCountry',             'uses' => 'Rent\Sergeant\Controllers\CountryController@createRecord',             'resource' => 'admin-country',          'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/countries/store/{lang}/{offset}/{id?}',             ['as' => 'storeCountry',              'uses' => 'Rent\Sergeant\Controllers\CountryController@storeRecord',              'resource' => 'admin-country',          'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/countries/{id}/edit/{lang}/{offset}',                ['as' => 'editCountry',               'uses' => 'Rent\Sergeant\Controllers\CountryController@editRecord',               'resource' => 'admin-country',          'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/countries/update/{lang}/{id}/{offset}',              ['as' => 'updateCountry',             'uses' => 'Rent\Sergeant\Controllers\CountryController@updateRecord',             'resource' => 'admin-country',          'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/countries/delete/{lang}/{id}/{offset}',              ['as' => 'deleteCountry',             'uses' => 'Rent\Sergeant\Controllers\CountryController@deleteRecord',             'resource' => 'admin-country',          'action' => 'delete']);
    Route::get(config('sergeant.appName') . '/sergeant/countries/delete/translation/{lang}/{id}/{offset}',  ['as' => 'deleteTranslationCountry',  'uses' => 'Rent\Sergeant\Controllers\CountryController@deleteTranslationRecord',  'resource' => 'admin-country',          'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/countries/delete/select/records/{lang}',          ['as' => 'deleteSelectCountry',       'uses' => 'Rent\Sergeant\Controllers\CountryController@deleteRecordsSelect',      'resource' => 'admin-country',          'action' => 'delete']);
    Route::post(config('sergeant.appName') . '/sergeant/countries/json/country/{country?}',                 ['as' => 'jsonGetCountry',            'uses' => 'Rent\Sergeant\Controllers\CountryController@jsonCountry',              'resource' => 'admin-country',          'action' => 'access']);

    /*
    |--------------------------------------------------------------------------
    | TERRITORIAL AREAS 1
    |--------------------------------------------------------------------------
    | URL jsonDataTerritorialArea1 has to go first to avoid enter in TerritorialArea1 when we call to jsonDataTerritorialArea1
    */
    Route::any(config('sergeant.appName') . '/sergeant/territorialareas1/json/data/{country}/{parentOffset}/{offset?}',             ['as' => 'jsonDataTerritorialArea1',              'uses' => 'Rent\Sergeant\Controllers\TerritorialArea1Controller@jsonData',                             'resource' => 'admin-country-at1',  'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/territorialareas1/{country}/{parentOffset}/{offset?}',                       ['as' => 'territorialArea1',                      'uses' => 'Rent\Sergeant\Controllers\TerritorialArea1Controller@index',                                'resource' => 'admin-country-at1',  'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/territorialareas1/create/{country}/{parentOffset}/{offset}',                 ['as' => 'createTerritorialArea1',                'uses' => 'Rent\Sergeant\Controllers\TerritorialArea1Controller@createRecord',                         'resource' => 'admin-country-at1',  'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/territorialareas1/store/{country}/{parentOffset}/{offset}',                 ['as' => 'storeTerritorialArea1',                 'uses' => 'Rent\Sergeant\Controllers\TerritorialArea1Controller@storeRecord',                          'resource' => 'admin-country-at1',  'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/territorialareas1/{id}/edit/{country}/{parentOffset}/{offset}',              ['as' => 'editTerritorialArea1',                  'uses' => 'Rent\Sergeant\Controllers\TerritorialArea1Controller@editRecord',                           'resource' => 'admin-country-at1',  'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/territorialareas1/update/{country}/{parentOffset}/{id}/{offset}',            ['as' => 'updateTerritorialArea1',                'uses' => 'Rent\Sergeant\Controllers\TerritorialArea1Controller@updateRecord',                         'resource' => 'admin-country-at1',  'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/territorialareas1/delete/{country}/{parentOffset}/{id}/{offset}',            ['as' => 'deleteTerritorialArea1',                'uses' => 'Rent\Sergeant\Controllers\TerritorialArea1Controller@deleteRecord',                         'resource' => 'admin-country-at1',  'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/territorialareas1/delete/select/records/{country}/{parentOffset}',        ['as' => 'deleteSelectTerritorialArea1',          'uses' => 'Rent\Sergeant\Controllers\TerritorialArea1Controller@deleteRecordsSelect',                  'resource' => 'admin-country-at1',  'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | TERRITORIAL AREAS 2
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/territorialareas2/json/data/{country}/{parentOffset}/{offset?}',             ['as' => 'jsonDataTerritorialArea2',              'uses' => 'Rent\Sergeant\Controllers\TerritorialArea2Controller@jsonData',                                  'resource' => 'admin-country-at2',  'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/territorialareas2/{country}/{parentOffset}/{offset?}',                       ['as' => 'territorialArea2',                      'uses' => 'Rent\Sergeant\Controllers\TerritorialArea2Controller@index',                                     'resource' => 'admin-country-at2',  'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/territorialareas2/create/{country}/{parentOffset}/{offset}',                 ['as' => 'createTerritorialArea2',                'uses' => 'Rent\Sergeant\Controllers\TerritorialArea2Controller@createRecord',                              'resource' => 'admin-country-at2',  'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/territorialareas2/store/{country}/{parentOffset}/{offset}',                 ['as' => 'storeTerritorialArea2',                 'uses' => 'Rent\Sergeant\Controllers\TerritorialArea2Controller@storeRecord',                               'resource' => 'admin-country-at2',  'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/territorialareas2/{id}/edit/{country}/{parentOffset}/{offset}',              ['as' => 'editTerritorialArea2',                  'uses' => 'Rent\Sergeant\Controllers\TerritorialArea2Controller@editRecord',                                'resource' => 'admin-country-at2',  'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/territorialareas2/update/{country}/{parentOffset}/{id}/{offset}',            ['as' => 'updateTerritorialArea2',                'uses' => 'Rent\Sergeant\Controllers\TerritorialArea2Controller@updateRecord',                              'resource' => 'admin-country-at2',  'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/territorialareas2/delete/{country}/{parentOffset}/{id}/{offset}',            ['as' => 'deleteTerritorialArea2',                'uses' => 'Rent\Sergeant\Controllers\TerritorialArea2Controller@deleteRecord',                              'resource' => 'admin-country-at2',  'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/territorialareas2/delete/select/records/{country}/{parentOffset}',        ['as' => 'deleteSelectTerritorialArea2',          'uses' => 'Rent\Sergeant\Controllers\TerritorialArea2Controller@deleteRecordsSelect',                       'resource' => 'admin-country-at2',  'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | TERRITORIAL AREAS 3
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/territorialareas3/json/data/country/{country}/{parentOffset}/{offset?}',     ['as' => 'jsonDataTerritorialArea3',              'uses' => 'Rent\Sergeant\Controllers\TerritorialArea3Controller@jsonData',                                     'resource' => 'admin-country-at3',  'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/territorialareas3/{country}/{parentOffset}/{offset?}',                       ['as' => 'territorialArea3',                      'uses' => 'Rent\Sergeant\Controllers\TerritorialArea3Controller@index',                                        'resource' => 'admin-country-at3',  'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/territorialareas3/create/{country}/{parentOffset}/{offset}',                 ['as' => 'createTerritorialArea3',                'uses' => 'Rent\Sergeant\Controllers\TerritorialArea3Controller@createRecord',                                 'resource' => 'admin-country-at3',  'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/territorialareas3/store/{country}/{parentOffset}/{offset}',                 ['as' => 'storeTerritorialArea3',                 'uses' => 'Rent\Sergeant\Controllers\TerritorialArea3Controller@storeRecord',                                  'resource' => 'admin-country-at3',  'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/territorialareas3/{id}/edit/{country}/{parentOffset}/{offset}',              ['as' => 'editTerritorialArea3',                  'uses' => 'Rent\Sergeant\Controllers\TerritorialArea3Controller@editRecord',                                   'resource' => 'admin-country-at3',  'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/territorialareas3/update/{country}/{parentOffset}/{id}/{offset}',            ['as' => 'updateTerritorialArea3',                'uses' => 'Rent\Sergeant\Controllers\TerritorialArea3Controller@updateRecord',                                 'resource' => 'admin-country-at3',  'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/territorialareas3/delete/{country}/{parentOffset}/{id}/{offset}',            ['as' => 'deleteTerritorialArea3',                'uses' => 'Rent\Sergeant\Controllers\TerritorialArea3Controller@deleteRecord',                                 'resource' => 'admin-country-at3',  'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/territorialareas3/delete/select/records/{country}/{parentOffset}',        ['as' => 'deleteSelectTerritorialArea3',          'uses' => 'Rent\Sergeant\Controllers\TerritorialArea3Controller@deleteRecordsSelect',                          'resource' => 'admin-country-at3',  'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | USER
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/users/{offset?}',                        ['as' => 'user',              'uses' => 'Rent\Sergeant\Controllers\UserController@index',                'resource' => 'admin-user',             'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/users/json/data',                        ['as' => 'jsonDataUser',      'uses' => 'Rent\Sergeant\Controllers\UserController@jsonData',             'resource' => 'admin-user',             'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/users/create/{offset}',                  ['as' => 'createUser',        'uses' => 'Rent\Sergeant\Controllers\UserController@createRecord',         'resource' => 'admin-user',             'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/users/store/{offset}',                  ['as' => 'storeUser',         'uses' => 'Rent\Sergeant\Controllers\UserController@storeRecord',          'resource' => 'admin-user',             'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/users/{id}/edit/{offset}',               ['as' => 'editUser',          'uses' => 'Rent\Sergeant\Controllers\UserController@editRecord',           'resource' => 'admin-user',             'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/users/update/{id}/{offset}',             ['as' => 'updateUser',        'uses' => 'Rent\Sergeant\Controllers\UserController@updateRecord',         'resource' => 'admin-user',             'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/users/delete/{id}/{offset}',             ['as' => 'deleteUser',        'uses' => 'Rent\Sergeant\Controllers\UserController@deleteRecord',         'resource' => 'admin-user',             'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/users/delete/select/records',         ['as' => 'deleteSelectUser',  'uses' => 'Rent\Sergeant\Controllers\UserController@deleteRecordsSelect',  'resource' => 'admin-user',             'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | EMAIL ACCOUNTS
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/email/accounts/{offset?}',                   ['as' => 'emailAccount',              'uses' => 'Rent\Sergeant\Controllers\EmailAccountController@index',                  'resource' => 'admin-email-account',    'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/email/accounts/json/data',                   ['as' => 'jsonDataEmailAccount',      'uses' => 'Rent\Sergeant\Controllers\EmailAccountController@jsonData',               'resource' => 'admin-email-account',    'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/email/accounts/create/{offset}',             ['as' => 'createEmailAccount',        'uses' => 'Rent\Sergeant\Controllers\EmailAccountController@createRecord',           'resource' => 'admin-email-account',    'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/email/accounts/store/{offset}',             ['as' => 'storeEmailAccount',         'uses' => 'Rent\Sergeant\Controllers\EmailAccountController@storeRecord',            'resource' => 'admin-email-account',    'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/email/accounts/{id}/edit/{offset}',          ['as' => 'editEmailAccount',          'uses' => 'Rent\Sergeant\Controllers\EmailAccountController@editRecord',             'resource' => 'admin-email-account',    'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/email/accounts/update/{id}/{offset}',        ['as' => 'updateEmailAccount',        'uses' => 'Rent\Sergeant\Controllers\EmailAccountController@updateRecord',           'resource' => 'admin-email-account',    'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/email/accounts/delete/{id}/{offset}',        ['as' => 'deleteEmailAccount',        'uses' => 'Rent\Sergeant\Controllers\EmailAccountController@deleteRecord',           'resource' => 'admin-email-account',    'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/email/accounts/delete/select/records',    ['as' => 'deleteSelectEmailAccount',  'uses' => 'Rent\Sergeant\Controllers\EmailAccountController@deleteRecordsSelect',    'resource' => 'admin-email-account',    'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | ATTACHMENT
    |--------------------------------------------------------------------------
    */
    Route::post(config('sergeant.appName') . '/sergeant/attachment/store/{object}/{lang}',          ['as' => 'storeAttachment',        'uses' => 'Rent\Sergeant\Controllers\AttachmentController@storeAttachment',            'resource' => 'sergeant',    'action' => 'create']);
    Route::put(config('sergeant.appName') . '/sergeant/attachment/update/{object}/{lang}/{id}',     ['as' => 'updateAttachment',       'uses' => 'Rent\Sergeant\Controllers\AttachmentController@apiUpdateAttachment',        'resource' => 'sergeant',    'action' => 'edit']);
    Route::put(config('sergeant.appName') . '/sergeant/attachment/update/{object}/{lang}',          ['as' => 'updatesAttachment',      'uses' => 'Rent\Sergeant\Controllers\AttachmentController@apiUpdatesAttachment',       'resource' => 'sergeant',    'action' => 'edit']);
    Route::delete(config('sergeant.appName') . '/sergeant/attachment/delete/{lang}/{id}',           ['as' => 'deleteAttachment',       'uses' => 'Rent\Sergeant\Controllers\AttachmentController@apiDeleteAttachment',        'resource' => 'sergeant',    'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/attachment/delete/tmp',                   ['as' => 'deleteTmpAttachment',    'uses' => 'Rent\Sergeant\Controllers\AttachmentController@apiDeleteTmpAttachment',     'resource' => 'sergeant',    'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | LIBRARY
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/library/{offset?}',                 ['as' => 'attachmentLibrary',                   'uses' => 'Rent\Sergeant\Controllers\AttachmentLibraryController@index',                  'resource' => 'admin-attachment-library',        'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/library/json/data',                 ['as' => 'jsonDataAttachmentLibrary',           'uses' => 'Rent\Sergeant\Controllers\AttachmentLibraryController@jsonData',               'resource' => 'admin-attachment-library',        'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/library/create/{offset}',           ['as' => 'createAttachmentLibrary',             'uses' => 'Rent\Sergeant\Controllers\AttachmentLibraryController@createRecord',           'resource' => 'admin-attachment-library',        'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/library/store/api',                ['as' => 'storeAttachmentLibrary',              'uses' => 'Rent\Sergeant\Controllers\AttachmentLibraryController@storeAttachmentLibrary', 'resource' => 'admin-attachment-library',        'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/library/{id}/edit/{offset}',        ['as' => 'editAttachmentLibrary',               'uses' => 'Rent\Sergeant\Controllers\AttachmentLibraryController@editRecord',             'resource' => 'admin-attachment-library',        'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/library/delete/{id}/{offset}',      ['as' => 'deleteAttachmentLibrary',             'uses' => 'Rent\Sergeant\Controllers\AttachmentLibraryController@deleteRecord',           'resource' => 'admin-attachment-library',        'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/library/delete/select/records',  ['as' => 'deleteSelectAttachmentLibrary',       'uses' => 'Rent\Sergeant\Controllers\AttachmentLibraryController@deleteRecordsSelect',    'resource' => 'admin-attachment-library',        'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | ATTACHMENT FAMILY
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/attachment/families/{offset?}',                  ['as' => 'attachmentFamily',                'uses' => 'Rent\Sergeant\Controllers\AttachmentFamilyController@index',                     'resource' => 'admin-attachment-family',        'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/attachment/families/json/data',                  ['as' => 'jsonDataAttachmentFamily',        'uses' => 'Rent\Sergeant\Controllers\AttachmentFamilyController@jsonData',                  'resource' => 'admin-attachment-family',        'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/attachment/families/create/{offset}',            ['as' => 'createAttachmentFamily',          'uses' => 'Rent\Sergeant\Controllers\AttachmentFamilyController@createRecord',              'resource' => 'admin-attachment-family',        'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/attachment/families/store/{offset}',            ['as' => 'storeAttachmentFamily',           'uses' => 'Rent\Sergeant\Controllers\AttachmentFamilyController@storeRecord',               'resource' => 'admin-attachment-family',        'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/attachment/families/{id}/edit/{offset}',         ['as' => 'editAttachmentFamily',            'uses' => 'Rent\Sergeant\Controllers\AttachmentFamilyController@editRecord',                'resource' => 'admin-attachment-family',        'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/attachment/families/update/{id}/{offset}',       ['as' => 'updateAttachmentFamily',          'uses' => 'Rent\Sergeant\Controllers\AttachmentFamilyController@updateRecord',              'resource' => 'admin-attachment-family',        'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/attachment/families/delete/{id}/{offset}',       ['as' => 'deleteAttachmentFamily',          'uses' => 'Rent\Sergeant\Controllers\AttachmentFamilyController@deleteRecord',              'resource' => 'admin-attachment-family',        'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/attachment/families/delete/select/records',   ['as' => 'deleteSelectAttachmentFamily',    'uses' => 'Rent\Sergeant\Controllers\AttachmentFamilyController@deleteRecordsSelect',       'resource' => 'admin-attachment-family',        'action' => 'delete']);
    Route::any(config('sergeant.appName') . '/sergeant/attachment/families/{id}/show/{api}',            ['as' => 'apiShowAttachmentFamily',         'uses' => 'Rent\Sergeant\Controllers\AttachmentFamilyController@showRecord',                'resource' => 'admin-attachment-family',        'action' => 'access']);

    /*
    |--------------------------------------------------------------------------
    | ATTACHMENT MIME
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/attachment/mimes/{offset?}',                  ['as' => 'attachmentMime',                'uses' => 'Rent\Sergeant\Controllers\AttachmentMimeController@index',                     'resource' => 'admin-attachment-mime',        'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/attachment/mimes/json/data',                  ['as' => 'jsonDataAttachmentMime',        'uses' => 'Rent\Sergeant\Controllers\AttachmentMimeController@jsonData',                  'resource' => 'admin-attachment-mime',        'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/attachment/mimes/create/{offset}',            ['as' => 'createAttachmentMime',          'uses' => 'Rent\Sergeant\Controllers\AttachmentMimeController@createRecord',              'resource' => 'admin-attachment-mime',        'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/attachment/mimes/store/{offset}',            ['as' => 'storeAttachmentMime',           'uses' => 'Rent\Sergeant\Controllers\AttachmentMimeController@storeRecord',               'resource' => 'admin-attachment-mime',        'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/attachment/mimes/{id}/edit/{offset}',         ['as' => 'editAttachmentMime',            'uses' => 'Rent\Sergeant\Controllers\AttachmentMimeController@editRecord',                'resource' => 'admin-attachment-mime',        'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/attachment/mimes/update/{id}/{offset}',       ['as' => 'updateAttachmentMime',          'uses' => 'Rent\Sergeant\Controllers\AttachmentMimeController@updateRecord',              'resource' => 'admin-attachment-mime',        'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/attachment/mimes/delete/{id}/{offset}',       ['as' => 'deleteAttachmentMime',          'uses' => 'Rent\Sergeant\Controllers\AttachmentMimeController@deleteRecord',              'resource' => 'admin-attachment-mime',        'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/attachment/mimes/delete/select/records',   ['as' => 'deleteSelectAttachmentMime',    'uses' => 'Rent\Sergeant\Controllers\AttachmentMimeController@deleteRecordsSelect',       'resource' => 'admin-attachment-mime',        'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | CONTENT BUILDER
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/contentbuilder/{package}/{theme}/edit/{input}',              ['as' => 'contentbuilder',            'uses' => 'Rent\Sergeant\Controllers\ContentBuilderController@index',         'resource' => 'admin',     'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/contentbuilder/action/saveimage',                            ['as' => 'contentbuilderSaveImage',   'uses' => 'Rent\Sergeant\Controllers\ContentBuilderController@saveImage',     'resource' => 'admin',     'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/contentbuilder/action/blocks/{theme}',                       ['as' => 'contentbuilderBlocks',      'uses' => 'Rent\Sergeant\Controllers\ContentBuilderController@getBlocks',     'resource' => 'admin',     'action' => 'access']);

    /*
    |--------------------------------------------------------------------------
    | FIELD GROUP
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/custom/fields/groups/{offset?}',                  ['as' => 'customFieldGroup',                'uses' => 'Rent\Sergeant\Controllers\CustomFieldGroupController@index',                     'resource' => 'admin-field-group',        'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/custom/fields/groups/json/data',                  ['as' => 'jsonDataCustomFieldGroup',        'uses' => 'Rent\Sergeant\Controllers\CustomFieldGroupController@jsonData',                  'resource' => 'admin-field-group',        'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/custom/fields/groups/create/{offset}',            ['as' => 'createCustomFieldGroup',          'uses' => 'Rent\Sergeant\Controllers\CustomFieldGroupController@createRecord',              'resource' => 'admin-field-group',        'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/custom/fields/groups/store/{offset}',            ['as' => 'storeCustomFieldGroup',           'uses' => 'Rent\Sergeant\Controllers\CustomFieldGroupController@storeRecord',               'resource' => 'admin-field-group',        'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/custom/fields/groups/{id}/edit/{offset}',         ['as' => 'editCustomFieldGroup',            'uses' => 'Rent\Sergeant\Controllers\CustomFieldGroupController@editRecord',                'resource' => 'admin-field-group',        'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/custom/fields/groups/update/{id}/{offset}',       ['as' => 'updateCustomFieldGroup',          'uses' => 'Rent\Sergeant\Controllers\CustomFieldGroupController@updateRecord',              'resource' => 'admin-field-group',        'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/custom/fields/groups/delete/{id}/{offset}',       ['as' => 'deleteCustomFieldGroup',          'uses' => 'Rent\Sergeant\Controllers\CustomFieldGroupController@deleteRecord',              'resource' => 'admin-field-group',        'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/custom/fields/groups/delete/select/records',   ['as' => 'deleteSelectCustomFieldGroup',    'uses' => 'Rent\Sergeant\Controllers\CustomFieldGroupController@deleteRecordsSelect',       'resource' => 'admin-field-group',        'action' => 'delete']);
    Route::any(config('sergeant.appName') . '/sergeant/custom/fields/groups/{id}/show/{api}',            ['as' => 'apiShowCustomFieldGroup',         'uses' => 'Rent\Sergeant\Controllers\CustomFieldGroupController@showRecord',                'resource' => 'admin-field-group',        'action' => 'access']);

    /*
    |--------------------------------------------------------------------------
    | FIELD
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/custom/fields/{lang}/{offset?}',                             ['as' => 'customField',                    'uses' => 'Rent\Sergeant\Controllers\CustomFieldController@index',                      'resource' => 'admin-field',        'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/custom/fields/json/data/{lang}',                             ['as' => 'jsonDataCustomField',            'uses' => 'Rent\Sergeant\Controllers\CustomFieldController@jsonData',                   'resource' => 'admin-field',        'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/custom/fields/create/{lang}/{offset}/{id?}',                 ['as' => 'createCustomField',              'uses' => 'Rent\Sergeant\Controllers\CustomFieldController@createRecord',               'resource' => 'admin-field',        'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/custom/fields/store/{lang}/{offset}/{id?}',                 ['as' => 'storeCustomField',               'uses' => 'Rent\Sergeant\Controllers\CustomFieldController@storeRecord',                'resource' => 'admin-field',        'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/custom/fields/{id}/edit/{lang}/{offset}',                    ['as' => 'editCustomField',                'uses' => 'Rent\Sergeant\Controllers\CustomFieldController@editRecord',                 'resource' => 'admin-field',        'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/custom/fields/update/{lang}/{id}/{offset}',                  ['as' => 'updateCustomField',              'uses' => 'Rent\Sergeant\Controllers\CustomFieldController@updateRecord',               'resource' => 'admin-field',        'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/custom/fields/delete/{lang}/{id}/{offset}',                  ['as' => 'deleteCustomField',              'uses' => 'Rent\Sergeant\Controllers\CustomFieldController@deleteRecord',               'resource' => 'admin-field',        'action' => 'delete']);
    Route::get(config('sergeant.appName') . '/sergeant/custom/fields/delete/translation/{lang}/{id}/{offset}',      ['as' => 'deleteTranslationCustomField',   'uses' => 'Rent\Sergeant\Controllers\CustomFieldController@deleteTranslationRecord',    'resource' => 'admin-field',        'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/custom/fields/delete/select/records/{lang}',              ['as' => 'deleteSelectCustomField',        'uses' => 'Rent\Sergeant\Controllers\CustomFieldController@deleteRecordsSelect',        'resource' => 'admin-field',        'action' => 'delete']);
    Route::post(config('sergeant.appName') . '/sergeant/custom/fields/api/get/custom/fields',                       ['as' => 'apiGetCustomFields',             'uses' => 'Rent\Sergeant\Controllers\CustomFieldController@apiGetCustomFields',         'resource' => 'admin-field',        'action' => 'access']);

    /*
    |--------------------------------------------------------------------------
    | FIELD VALUE
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/custom/fields/values/{field}/{lang}/{offset?}',                          ['as' => 'customFieldValue',                    'uses' => 'Rent\Sergeant\Controllers\CustomFieldValueController@index',                      'resource' => 'admin-field-value',        'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/custom/fields/values/json/data/{field}/{lang}',                          ['as' => 'jsonDataCustomFieldValue',            'uses' => 'Rent\Sergeant\Controllers\CustomFieldValueController@jsonData',                   'resource' => 'admin-field-value',        'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/custom/fields/values/create/{field}/{lang}/{offset}/{id?}',              ['as' => 'createCustomFieldValue',              'uses' => 'Rent\Sergeant\Controllers\CustomFieldValueController@createRecord',               'resource' => 'admin-field-value',        'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/custom/fields/values/store/{field}/{lang}/{offset}/{id?}',              ['as' => 'storeCustomFieldValue',               'uses' => 'Rent\Sergeant\Controllers\CustomFieldValueController@storeRecord',                'resource' => 'admin-field-value',        'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/custom/fields/values/{field}/{id}/edit/{lang}/{offset}',                 ['as' => 'editCustomFieldValue',                'uses' => 'Rent\Sergeant\Controllers\CustomFieldValueController@editRecord',                 'resource' => 'admin-field-value',        'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/custom/fields/values/update/{field}/{lang}/{id}/{offset}',               ['as' => 'updateCustomFieldValue',              'uses' => 'Rent\Sergeant\Controllers\CustomFieldValueController@updateRecord',               'resource' => 'admin-field-value',        'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/custom/fields/values/delete/{field}/{lang}/{id}/{offset}',               ['as' => 'deleteCustomFieldValue',              'uses' => 'Rent\Sergeant\Controllers\CustomFieldValueController@deleteRecord',               'resource' => 'admin-field-value',        'action' => 'delete']);
    Route::get(config('sergeant.appName') . '/sergeant/custom/fields/values/delete/translation/{field}/{lang}/{id}/{offset}',   ['as' => 'deleteTranslationCustomFieldValue',   'uses' => 'Rent\Sergeant\Controllers\CustomFieldValueController@deleteTranslationRecord',    'resource' => 'admin-field-value',        'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/custom/fields/values/delete/select/records/{field}/{lang}',           ['as' => 'deleteSelectCustomFieldValue',        'uses' => 'Rent\Sergeant\Controllers\CustomFieldValueController@deleteRecordsSelect',        'resource' => 'admin-field-value',        'action' => 'delete']);

    /*
    |--------------------------------------------------------------------------
    | FROALA
    |--------------------------------------------------------------------------
    */
    Route::post(config('sergeant.appName') . '/sergeant/froala/upload/image',                                       ['as' => 'froalaUploadImage',             'uses' => 'Rent\Sergeant\Controllers\FroalaController@uploadImage']);
    Route::get(config('sergeant.appName') . '/sergeant/froala/load/images/{package}',                               ['as' => 'froalaLoadImages',              'uses' => 'Rent\Sergeant\Controllers\FroalaController@loadImages']);
    Route::post(config('sergeant.appName') . '/sergeant/froala/delete/image',                                       ['as' => 'froalaDeleteImage',             'uses' => 'Rent\Sergeant\Controllers\FroalaController@deleteImage']);
    Route::post(config('sergeant.appName') . '/sergeant/froala/upload/file',                                        ['as' => 'froalaUploadFile',              'uses' => 'Rent\Sergeant\Controllers\FroalaController@uploadFile']);
    
    /*
    |--------------------------------------------------------------------------
    | PLAN
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/plans/{offset?}',                        ['as' => 'plan',              'uses' => 'Rent\Sergeant\Controllers\PlanController@index',           'resource' => 'admin-plan',  'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/plans/json/data',                        ['as' => 'jsonDataPlan',      'uses' => 'Rent\Sergeant\Controllers\PlanController@jsonData',             'resource' => 'admin-plan',             'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/plans/create/{offset}',                  ['as' => 'createPlan',        'uses' => 'Rent\Sergeant\Controllers\PlanController@createRecord',         'resource' => 'admin-plan',             'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/plans/store/{offset}',                  ['as' => 'storePlan',         'uses' => 'Rent\Sergeant\Controllers\PlanController@storeRecord',          'resource' => 'admin-plan',             'action' => 'create']);
     Route::post(config('sergeant.appName') . '/sergeant/plans/store/{offset}',                  ['as' => 'storePlan',         'uses' => 'Rent\Sergeant\Controllers\PlanController@store',          'resource' => 'admin-plan',             'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/plans/{id}/edit/{offset}',               ['as' => 'editPlan',          'uses' => 'Rent\Sergeant\Controllers\PlanController@editRecord',           'resource' => 'admin-plan',             'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/plans/update/{id}/{offset}',             ['as' => 'updatePlan',        'uses' => 'Rent\Sergeant\Controllers\PlanController@updateRecord',         'resource' => 'admin-plan',             'action' => 'edit']);
    Route::put(config('sergeant.appName') . '/sergeant/plans/update/{id}/{offset}',             ['as' => 'updatePlan',        'uses' => 'Rent\Sergeant\Controllers\PlanController@update',         'resource' => 'admin-plan',             'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/plans/delete/{id}/{offset}',             ['as' => 'deletePlan',        'uses' => 'Rent\Sergeant\Controllers\PlanController@deleteRecord',         'resource' => 'admin-plan',             'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/plans/delete/select/records',         ['as' => 'deleteSelectPlan',  'uses' => 'Rent\Sergeant\Controllers\PlanController@deleteRecordsSelect',  'resource' => 'admin-plan',             'action' => 'delete']);

    
    /*
    |--------------------------------------------------------------------------
    | CURRENCY
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/sergeant/currency/{offset?}',                        ['as' => 'currency',              'uses' => 'Rent\Sergeant\Controllers\CurrencyController@index',           'resource' => 'admin-currency',  'action' => 'access']);
    Route::any(config('sergeant.appName') . '/sergeant/currency/json/data',                        ['as' => 'jsonDataCurrency',      'uses' => 'Rent\Sergeant\Controllers\CurrencyController@jsonData',             'resource' => 'admin-currency',             'action' => 'access']);
    Route::get(config('sergeant.appName') . '/sergeant/currency/create/{offset}',                  ['as' => 'createCurrency',        'uses' => 'Rent\Sergeant\Controllers\CurrencyController@createRecord',         'resource' => 'admin-currency',             'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/currency/store/{offset}',                  ['as' => 'storeCurrency',         'uses' => 'Rent\Sergeant\Controllers\CurrencyController@storeRecord',          'resource' => 'admin-currency',             'action' => 'create']);
    Route::post(config('sergeant.appName') . '/sergeant/currency/store/{offset}',                  ['as' => 'storeCurrency',         'uses' => 'Rent\Sergeant\Controllers\CurrencyController@store',          'resource' => 'admin-currency',             'action' => 'create']);
    Route::get(config('sergeant.appName') . '/sergeant/currency/{id}/edit/{offset}',               ['as' => 'editCurrency',          'uses' => 'Rent\Sergeant\Controllers\CurrencyController@editRecord',           'resource' => 'admin-currency',             'action' => 'access']);
    Route::put(config('sergeant.appName') . '/sergeant/currency/update/{id}/{offset}',             ['as' => 'updateCurrency',        'uses' => 'Rent\Sergeant\Controllers\CurrencyController@updateRecord',         'resource' => 'admin-currency',             'action' => 'edit']);
    Route::put(config('sergeant.appName') . '/sergeant/currency/update/{id}/{offset}',             ['as' => 'updateCurrency',        'uses' => 'Rent\Sergeant\Controllers\CurrencyController@update',         'resource' => 'admin-currency',             'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/sergeant/currency/delete/{id}/{offset}',             ['as' => 'deleteCurrency',        'uses' => 'Rent\Sergeant\Controllers\CurrencyController@deleteRecord',         'resource' => 'admin-currency',             'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/sergeant/currency/delete/select/records',         ['as' => 'deleteSelectCurrency',  'uses' => 'Rent\Sergeant\Controllers\CurrencyController@deleteRecordsSelect',  'resource' => 'admin-currency',             'action' => 'delete']);

});

