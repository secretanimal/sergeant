@extends('sergeant::layouts.form')

@section('rows')
    <!-- sergeant::actions.common -->
    @include('sergeant::includes.html.form_text_group', [
        'label' => 'ID',
        'name' => 'id',
        'value' => old('id', isset($object)? $object->id_008 : null),
        'maxLength' => '25',
        'rangeLength' => '2,25',
        'required' => true,
        'fieldSize' => 2
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.name'),
        'name' => 'name',
        'value' => old('name', isset($object)? $object->name_008 : null),
        'maxLength' => '255',
        'rangeLength' => '2,255',
        'required' => true
    ])
    <!-- /sergeant::actions.common -->
@stop