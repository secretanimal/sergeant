@extends('sergeant::layouts.index', ['newTrans' => 'new2'])

@section('head')
    @parent
    <!-- sergeant::actions.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [2,3]},
                        { "class": "checkbox-column", "targets": [2]},
                        { "class": "align-center", "targets": [3]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix)) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.sergeant::actions.index -->
@stop

@section('tHead')
    <!-- sergeant::actions.index -->
    <th data-hide="phone,tablet">ID.</th>
    <th data-class="expand">{{ trans('sergeant::sergeant.name') }}</th>
    <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
    <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    <!-- /.sergeant::actions.index -->
@stop