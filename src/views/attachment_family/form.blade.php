@extends('sergeant::layouts.form')

@section('rows')
    <!-- sergeant::attachment_family.form -->
    @include('sergeant::includes.html.form_text_group', [
        'label' => 'ID',
        'name' => 'id',
        'value' => isset($object)? $object->id_015 : null,
        'fieldSize' => 2,
        'readOnly' => true
    ])
    @include('sergeant::includes.html.form_select_group', [
        'label' => trans_choice('sergeant::sergeant.resource', 1),
        'id' => 'resource', 'name' => 'resource',
        'value' => old('resource', isset($object)? $object->resource_id_015 : null),
        'objects' => $resources,
        'idSelect' => 'id_007',
        'nameSelect' => 'name_007',
        'class' => 'select2',
        'fieldSize' => 5,
        'required' => true,
        'data' => [
            'language' => config('app.locale'),
            'width' => '100%',
            'error-placement' => 'select2-resource-outer-container'
        ]
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.name'),
        'name' => 'name',
        'value' => old('name', isset($object)? $object->name_015 : null),
        'maxLength' => '255',
        'rangeLength' => '2,255',
        'required' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.width'),
        'type' => 'number',
        'name' => 'width',
        'value' => old('width', isset($object)? $object->width_015 : null),
        'fieldSize' => 4,
        'inputs' => [
            [
                'label' => trans('sergeant::sergeant.height'),
                'type' => 'number',
                'name' => 'height',
                'value' => old('height', isset($object)? $object->height_015 : null),
                'fieldSize' => 4
            ]
    ]])
    <!-- /.sergeant::attachment_family.form -->
@stop