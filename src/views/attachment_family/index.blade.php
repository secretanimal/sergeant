@extends('sergeant::layouts.index')

@section('head')
    @parent
    <!-- sergeant::attachment_family.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [5,6]},
                        { "class": "checkbox-column", "targets": [5]},
                        { "class": "align-center", "targets": [3,4,6]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix)) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.sergeant::attachment_family.index -->
@stop

@section('tHead')
    <!-- sergeant::attachment_family.index -->
    <tr>
        <th data-hide="phone,tablet">ID.</th>
        <th data-hide="phone">{{ trans_choice('sergeant::sergeant.resource', 1) }}</th>
        <th data-class="expand">{{ trans('sergeant::sergeant.name') }}</th>
        <th>{{ trans('sergeant::sergeant.width') }} (px)</th>
        <th>{{ trans('sergeant::sergeant.height') }} (px)</th>
        <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
        <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    </tr>
    <!-- /.sergeant::attachment_family.index -->
@stop