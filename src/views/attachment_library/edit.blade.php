@extends('sergeant::layouts.form', ['action' => 'update'])

@section('head')
    @parent
    @include('sergeant::includes.js.delete_translation_record')
@stop

@section('rows')
    <!-- cms::library.edit -->
    @include('sergeant::includes.html.form_text_group', ['label' => 'ID', 'name' => 'id',  'value' => $object->id_352, 'readOnly' => true, 'fieldSize' => 2])
    @include('sergeant::includes.html.form_image_group', ['label' => trans_choice('sergeant::sergeant.language', 1), 'name' => 'lang', 'nameImage' => $lang->name_001, 'value' => $lang->id_001, 'url' => asset('/packages/rent/sergeant/storage/langs/' . $lang->image_001)])
    @include('sergeant::includes.html.form_text_group', ['label' => trans('sergeant::sergeant.name'), 'name' => 'name', 'value' => $object->name_352, 'maxLength' => '50', 'rangeLength' => '2,50', 'required' => true])
    @include('sergeant::includes.html.form_text_group', ['label' => trans('sergeant::sergeant.sorting'), 'name' => 'sorting', 'type' => 'number', 'value' => $object->sorting_352, 'maxLength' => '3', 'rangeLength' => '1,3', 'min' => '0', 'fieldSize' => 2])
    <!-- /cms::library.edit -->
@stop