@extends('sergeant::layouts.form')

@section('rows')
    <!-- sergeant::attachment_library.create -->
    @include('sergeant::includes.html.form_text_group', [
        'label' => 'ID',
        'name' => 'id',
        'value' => old('name', isset($object->id_352)? $object->id_352 : null),
        'readOnly' => true,
        'fieldSize' => 2
    ])
    @include('sergeant::includes.html.form_image_group', [
        'label' => trans_choice('sergeant::sergeant.language', 1),
        'name' => 'lang',
        'nameImage' => $lang->name_001,
        'value' => $lang->id_001,
        'url' => asset('/packages/rent/sergeant/storage/langs/' . $lang->image_001)
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.name'),
        'name' => 'name',
        'value' => old('name', isset($object->name_352)? $object->name_352 : null),
        'maxLength' => '50',
        'rangeLength' => '2,50',
        'required' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.sorting'),
        'name' => 'sorting',
        'type' => 'number',
        'value' => old('sorting', isset($object->sorting_352)? $object->sorting_352 : null),
        'maxLength' => '3',
        'rangeLength' => '1,3',
        'min' => '0',
        'fieldSize' => 2
    ])
    <!-- /.sergeant::attachment_library.create -->
@stop