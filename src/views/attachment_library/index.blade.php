@extends('sergeant::layouts.index')

@section('head')
    @parent
    <!-- sergeant::attachment_library.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [6,7]},
                        { "class": "checkbox-column", "targets": [6]},
                        { "class": "align-center", "targets": [1,7]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix)) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.sergeant::attachment_library.index -->
@stop

@section('tHead')
    <!-- sergeant::attachment_library.index -->
    <tr>
        <th data-hide="phone,tablet">ID.</th>
        <th data-hide="phone,tablet">{{ trans_choice('sergeant::sergeant.file', 1) }}</th>
        <th>{{ trans('sergeant::sergeant.name') }}</th>
        <th data-class="expand">{{ trans_choice('sergeant::sergeant.size', 1) }}</th>
        <th data-hide="phone,tablet">MIME</th>
        <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.type') }}</th>
        <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
        <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    </tr>
    <!-- /.sergeant::attachment_library.index -->
@stop