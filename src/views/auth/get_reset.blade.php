@extends('sergeant::layouts.login')

@section('head')
    <!-- App -->
    <script src="{{ asset('packages/rent/sergeant/js/login.js') }}"></script>
    <script>
        $(document).ready(function() {
            
            // passwors has reset
            @if(session('status') !== null)
                new PNotify({
                    type:   'success',
                    title:  '{{ trans('sergeant::sergeant.reset_password') }}',
                    text:   '{{ session('status') }}',
                    opacity: .9,
                    styling: 'fontawesome'
                });
            @endif
        });
    </script>
    @include('sergeant::includes.js.header_form')
    
@stop

@section('mainContent')

        <!-- Forgot Password Formular -->
        <form id="forgot-password" class="form-vertical forgot-password-form" action="{{ route('sendResetLinkEmail') }}" method="post" onsubmit="return false">
            {{ csrf_field() }}

            <h3 class="form-title">{{ trans('sergeant::sergeant.reset_password') }}</h3>

            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-at"></i>
                    <!--<input type="email" name="email_010" class="form-control" placeholder="{{ trans('sergeant::sergeant.enter_email') }}" data-rule-required="true" data-rule-email="true" data-msg-required="{{ trans('sergeant::sergeant.error_reset_password') }}">-->
                    <input type="email" name="email_010" class="form-control" placeholder="user@domain.com" data-rule-required="true" data-rule-email="true" data-msg-required="{{ trans('sergeant::sergeant.error_reset_password') }}">
                </div>
            </div>

            <button type="submit" class="submit btn btn-default btn-block rs-btn-reset">
                {{ trans('sergeant::sergeant.reset_password') }} <i class="fa fa-refresh"></i>
            </button>

            <div class="form-group">
            
                <div class="col-xs-6">
                    <a href="{{ route('sergeantGetLogin') }}" class="link pull-left">Iniciar sesi&oacute;n</a>
                </div>
                <div class="col-xs-6 ">
                    <a href="{{ route('sergeantGetRegister') }}" class="link pull-right">Crear una cuenta</a>
                </div>

            </div>
        </form>
        <!-- /Forgot Password Formular -->

        <!-- Shows up if reset-button was clicked -->
        <div class="forgot-password-done hide-default">
            <div class="forgot-password-ok">
                <i class="fa fa-check success-icon"></i>
                <span>{{ trans('sergeant::sergeant.reset_password_successful') }}</span>
            </div>
            <div class="forgot-password-nook">
                <i class="fa fa-remove danger-icon"></i>
                <span>{{ trans('sergeant::sergeant.error2_reset_password') }}</span>
            </div>
        </div>
     
@stop