@extends('sergeant::layouts.login')

@section('head')
    <!-- App -->
    <script src="{{ asset('packages/rent/sergeant/js/login.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if(isset($errors) && $errors->has('loginErrors') && $errors->first('loginErrors') == 1)
                new PNotify({
                    type:   'error',
                    title:  '{{ trans('sergeant::sergeant.message_error_login') }}',
                    text:   '{{ trans('sergeant::sergeant.message_error_login_msg_1') }}',
                    opacity: .9,
                    styling: 'fontawesome'
                });
            @endif

            @if(isset($errors) && $errors->has('loginErrors') && $errors->first('loginErrors') == 2)
                new PNotify({
                    type:   'error',
                    title:  '{{ trans('sergeant::sergeant.message_error_login') }}',
                    text:   '{{ trans('sergeant::sergeant.message_error_login_msg_2') }}',
                    opacity: .9,
                    styling: 'fontawesome'
                });
            @endif

            @if(isset($errors) && $errors->has('loginErrors') && $errors->first('loginErrors') == 3)
                new PNotify({
                    type:   'error',
                    title:  '{{ trans('sergeant::sergeant.message_error_login') }}',
                    text:   '{{ trans('sergeant::sergeant.message_error_login_msg_3') }}',
                    opacity: .9,
                    styling: 'fontawesome'
                });
            @endif
            
            // register email success
            @if(session('status') !== null && session('status') == 'register_success')
                new PNotify({
                    type:   'success',
                    title:  '{{ trans('sergeant::sergeant.register_status') }}',
                    text:   '{{ trans('sergeant::sergeant.send_email') }}',
                    opacity: .9,
                    styling: 'fontawesome'
                });
            @endif
            // register email error
            @if(session('status') !== null && session('status') == 'register_error')
                new PNotify({
                    type:   'success',
                    title:  '{{ trans('sergeant::sergeant.register_status') }}',
                    text:   '{{ trans('sergeant::sergeant.fail_sending_email') }}',
                    opacity: .9,
                    styling: 'fontawesome'
                });
            @endif         
            // confirm email status
            @if(session('status') !== null && session('status') == 'confirm_status')
                new PNotify({
                    type:   'success',
                    title:  '{{ trans('sergeant::sergeant.confirm_status') }}',
                    text:   '{{ trans('sergeant::sergeant.confirm_success') }}',
                    opacity: .9,
                    styling: 'fontawesome'
                });
            @endif
        });
    </script>
@stop

@section('mainContent')
<!-- Login Formular -->
<form class="form-vertical login-form" action="{{ route('sergeantPostLogin') }}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <h3 class="form-title">{{ trans('sergeant::sergeant.access_your_account') }}</h3>

    <!-- Error Message -->
    <!--
    <div class="alert fade in alert-danger" style="display: none;">
        <i class="icon-remove close" data-dismiss="alert"></i>
        {{ trans('sergeant::sergeant.message_user_pass') }}
    </div>-->

    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-user"></i>
            <input type="email" name="user_010" value="{{ old('user_010') }}" class="form-control" placeholder="user@domain.com" autofocus="autofocus" data-rule-required="true" data-msg-required="{{ trans('sergeant::sergeant.message_user') }}">
            <!--<input type="text" name="user_010" value="{{ old('user_010') }}" class="form-control" placeholder="{{ trans_choice('sergeant::sergeant.user', 1) }}" autofocus="autofocus" data-rule-required="true" data-msg-required="{{ trans('sergeant::sergeant.message_user') }}">-->
        </div>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input type="password" name="password" class="form-control" placeholder="{{ trans('sergeant::sergeant.password') }}" data-rule-required="true" data-msg-required="{{ trans('sergeant::sergeant.message_pass') }}">
        </div>
    </div>    
    <div class="form-actions">
        <button type="submit" class="submit btn rs-btn-login">
            {{ trans('sergeant::sergeant.login') }} <i class="fa fa-sign-in"></i>
        </button>
    </div>

    <div class="form-group">
        <div class="col-xs-6">
            <a href="{{ route('sergeantGetRegister') }}" class="link pull-left">Crear una cuenta</a>
        </div>
        <div class="col-xs-6 ">
            <a href="{{ route('sergeantGetReset') }}" class="link pull-right">¿Olvid&oacute; su clave?</a>
        </div>
    </div>
</form>
<!-- /Login Form -->
@stop

@section('reminder')
<div class="inner-box" style="display: none;">
    <div class="content">
        <!-- Close Button -->
        <i class="fa fa-remove close hide-default"></i>

        <!-- Link as Toggle Button -->
        <a href="#" class="forgot-password-link">{{ trans('sergeant::sergeant.remember_password') }}</a>

        <!-- Forgot Password Formular -->
        <form id="forgot-password" class="form-vertical forgot-password-form hide-default" action="{{ route('sendResetLinkEmail') }}" method="post" onsubmit="return false">
            {{ csrf_field() }}

            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-envelope"></i>
                    <input type="email" name="email_010" class="form-control" placeholder="{{ trans('sergeant::sergeant.enter_email') }}" data-rule-required="true" data-rule-email="true" data-msg-required="{{ trans('sergeant::sergeant.error_reset_password') }}">
                </div>
            </div>

            <button type="submit" class="submit btn btn-default btn-block">{{ trans('sergeant::sergeant.reset_password') }}</button>
        </form>
        <!-- /Forgot Password Formular -->

        <!-- Shows up if reset-button was clicked -->
        <div class="forgot-password-done hide-default">
            <div class="forgot-password-ok">
                <i class="fa fa-check success-icon"></i>
                <span>{{ trans('sergeant::sergeant.reset_password_successful') }}</span>
            </div>
            <div class="forgot-password-nook">
                <i class="fa fa-remove danger-icon"></i>
                <span>{{ trans('sergeant::sergeant.error2_reset_password') }}</span>
            </div>
        </div>
    </div>
</div>
@stop