@extends('sergeant::layouts.login')

@section('head')
    <script src="{{ asset('packages/rent/sergeant/js/login.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if(isset($errors) && $errors->has('registerErrors') && $errors->first('registerErrors') == 1)
                new PNotify({
                    type:   'error',
                    title:  '{{ trans('sergeant::sergeant.message_error_register') }}',
                    text:   '{{ trans('sergeant::sergeant.message_error_register_msg_1') }}',
                    opacity: .9,
                    styling: 'fontawesome'
                });
            @endif
        });
    </script>
    @include('sergeant::includes.js.header_form')
    
@stop

@section('mainContent')
<!-- Register Formulari -->
<form class="form-vertical login-form" action="{{ route('sergeantPostRegister') }}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <h3 class="form-title">{{ trans('sergeant::sergeant.register') }}</h3>

    <!-- Error Message -->
    <!--<div class="alert fade in alert-danger" style="display: none;">
        <i class="icon-remove close" data-dismiss="alert"></i>
        {{ trans('sergeant::sergeant.message_user_pass') }}
    </div>-->
   
    <div class="form-group {{ $errors->has('id_033') ? 'has-error' : ''}}">
                     
                {!! Form::label('plan', trans('sergeant::sergeant.plan'), ['class' => 'form-control']) !!}
                <div class="col-sm-4 rs-register-div-plan">
                    {!! Form::select('plan', $plan, null, ['id' => 'id_033','class'=>'rs-register-select-plan']) !!}
                    {!! $errors->first('id_033', '<p class="help-block">:message</p>') !!}
  
                </div>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-building-o"></i>
            <input type="text" name="name_029" value="{{ old('name_029') }}" class="form-control" placeholder="{{ trans('sergeant::sergeant.plaza') }}" autofocus="autofocus" data-rule-required="true" data-msg-required="{{ trans('sergeant::sergeant.message_plaza') }}">
        </div>
    </div>

    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-user"></i>
            <input type="text" name="name_010" value="{{ old('name_010') }}" class="form-control" placeholder="{{ trans('sergeant::sergeant.name') }}" data-rule-required="true" data-msg-required="{{ trans('sergeant::sergeant.message_name') }}">
        </div>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-user"></i>
            <input type="text" name="surname_010" value="{{ old('surname_010') }}" class="form-control" placeholder="{{ trans('sergeant::sergeant.surname') }}" data-rule-required="true" data-msg-required="{{ trans('sergeant::sergeant.message_surname') }}">
        </div>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-at"></i>
    <input type="email" class="form-control" name="email_010" value="{{ old('email_010') }}" placeholder="{{ trans('sergeant::sergeant.email') }}" data-rule-required="true" data-msg-required="{{ trans('sergeant::sergeant.message_email') }}">
        </div>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input type="password" name="password" class="form-control" placeholder="{{ trans('sergeant::sergeant.password') }}" data-rule-required="true" data-msg-required="{{ trans('sergeant::sergeant.message_pass') }}">
        </div>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input type="password" name="password_confirmation" class="form-control" placeholder="{{ trans('sergeant::sergeant.re_password') }}" data-rule-required="true" data-msg-required="{{ trans('sergeant::sergeant.message_repass') }}">
        </div>
    </div>
    <div class="form-actions">
        <button type="submit" class="submit btn rs-btn-register">
            {{ trans('sergeant::sergeant.register') }} <i class="fa fa-pencil-square-o"></i>
        </button>
    </div>

    <div class="form-group">
        <div class="col-xs-6">
            <a href="{{ route('sergeantGetLogin') }}" class="link pull-left">Iniciar sesi&oacute;n</a>
        </div>
        <div class="col-xs-6 ">
            <a href="{{ route('sergeantGetReset') }}" class="link pull-right">¿Olvid&oacute; su clave?</a>
        </div>
    </div>
</form>
<!-- /Register Form -->
@stop