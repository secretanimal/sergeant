@extends('sergeant::layouts.login')

@section('head')
    <script src="{{ asset('packages/rent/sergeant/js/login.js') }}"></script>
    @include('sergeant::includes.js.header_form')
@stop

@section('mainContent')
<form class="form-vertical login-form" action="{{ route('postResetPassword', ['token' => $token]) }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="token" value="{{ $token }}">

    <h3 class="form-title">{{ trans('sergeant::sergeant.reset_password') }}</h3>

    <!-- Error Message -->
    <div class="alert fade in alert-danger" style="display: none;">
        <i class="icon-remove close" data-dismiss="alert"></i>
        Enter your email and new password
    </div>
    <!-- /Error Message -->
    <div class="form-group">
        <div class="input-icon">
            <i class="icon-envelope"></i>
            <input type="email" name="email_010" class="form-control required" placeholder="{{ trans('sergeant::sergeant.email') }}" autofocus="autofocus">
            {!! $errors->first('email_010', config('sergeant.errorDelimiters')) !!}
            {!! $errors->first('token', config('sergeant.errorDelimiters')) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="icon-lock"></i>
            <input type="password" name="password" class="form-control required" placeholder="{{ trans('sergeant::sergeant.password') }}">
            {!! $errors->first('password', config('sergeant.errorDelimiters')) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="icon-lock"></i>
            <input type="password" name="password_confirmation" class="form-control required" placeholder="{{ trans('sergeant::sergeant.repeat_password') }}" equalTo="[name='password']">
            {!! $errors->first('password_confirmation', config('sergeant.errorDelimiters')) !!}
        </div>
    </div>

    <div class="form-actions">
        <button type="submit" class="submit btn btn-primary pull-right">
            {{ trans('sergeant::sergeant.reset_password') }} <i class="icon-angle-right"></i>
        </button>
    </div>
</form>
@stop