@extends('sergeant::layouts.form')

@section('head')
    @parent
    @include('sergeant::includes.js.delete_translation_record')
@stop

@section('rows')
    <!-- sergeant::country.form -->
    @include('sergeant::includes.html.form_text_group', [
        'label' => 'ID',
        'name' => 'id',
        'value' => old('id', isset($object->id_002)? $object->id_002 : null),
        'maxLength' => '2',
        'size' => '2',
        'required' => true,
        'fieldSize' => 2,
        'readOnly' => $lang->id_001 == base_lang()->id_001? false : true
    ])
    @include('sergeant::includes.html.form_image_group', [
        'label' => trans_choice('sergeant::sergeant.language', 1),
        'name' => 'lang',
        'nameImage' => $lang->name_001,
        'value' => $lang->id_001,
        'url' => asset('/packages/rent/sergeant/storage/langs/' . $lang->image_001)
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.name'),
        'name' => 'name',
        'value' => old('name', isset($object->name_002)? $object->name_002 : null),
        'maxLength' => '50',
        'rangeLength' => '2,50',
        'required' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.sorting'),
        'name' => 'sorting',
        'value' => old('sorting', isset($object->sorting_002)? $object->sorting_002 : null),
        'maxLength' => '3',
        'rangeLength' => '1,3',
        'min' => '0',
        'fieldSize' => 2
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.prefix'),
        'name' => 'prefix',
        'value' => old('prefix', isset($object->prefix_002)? $object->prefix_002 : null),
        'maxLength' => '3',
        'rangeLength' => '1,3',
        'min' => '0', 'fieldSize' => 2
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans_choice('sergeant::sergeant.territorial_area',1) . ' 1',
        'name' => 'territorialArea1',
        'value' => old('territorialArea1', isset($object->territorial_area_1_002)? $object->territorial_area_1_002 : null),
        'maxLength' => '50',
        'rangeLength' => '2,50'
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans_choice('sergeant::sergeant.territorial_area',1) . ' 2',
        'name' => 'territorialArea2',
        'value' => old('territorialArea2', isset($object->territorial_area_2_002)? $object->territorial_area_2_002 : null),
        'maxLength' => '50',
        'rangeLength' => '2,50'
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans_choice('sergeant::sergeant.territorial_area',1) . ' 3',
        'name' => 'territorialArea3',
        'value' => old('territorialArea3', isset($object->territorial_area_3_002)? $object->territorial_area_3_002 : null),
        'maxLength' => '50',
        'rangeLength' => '2,50'
    ])
    <!-- /.sergeant::country.create -->
@stop