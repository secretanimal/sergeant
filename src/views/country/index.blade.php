@extends('sergeant::layouts.index')

@section('head')
    @parent
    <!-- sergeant::countries.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [8,9]},
                        { "class": "checkbox-column", "targets": [8]},
                        { "class": "align-center", "targets": [9]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix), [base_lang()->id_001]) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.sergeant::countries.index -->
@stop

@section('tHead')
    <!-- sergeant::countries.index -->
    <th data-hide="phone,tablet">ID.</th>
    <th data-hide="phone,tablet">{{ trans_choice('sergeant::sergeant.language', 1) }}</th>
    <th data-class="expand">{{ trans_choice('sergeant::sergeant.country', 1) }}</th>
    <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.sorting') }}</th>
    <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.prefix') }}</th>
    <th data-hide="phone">{{ trans_choice('sergeant::sergeant.territorial_area', 1) }} 1</th>
    <th data-hide="phone">{{ trans_choice('sergeant::sergeant.territorial_area', 1) }} 2</th>
    <th data-hide="phone">{{ trans_choice('sergeant::sergeant.territorial_area', 1) }} 3</th>
    <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
    <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    <!-- /.sergeant::countries.index -->
@stop