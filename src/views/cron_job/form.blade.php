@extends('sergeant::layouts.form')

@section('rows')
    <!-- sergeant::cron_jobs.form -->
    @include('sergeant::includes.html.form_text_group', [
        'label' => 'ID',
        'name' => 'id',
        'value' => isset($object->id_011)? $object->id_011 : null,
        'fieldSize' => 2,
        'readOnly' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.name'),
        'name' => 'name',
        'value' => old('name', isset($object->name_011)? $object->name_011 : null),
        'maxLength' => '255',
        'rangeLength' => '2,255',
        'required' => true
    ])
    @include('sergeant::includes.html.form_select_group', [
        'fieldSize' => 5,
        'label' => trans_choice('sergeant::sergeant.package', 1),
        'name' => 'package',
        'value' => (int) old('package', isset($object->package_id_011)? $object->package_id_011 : null),
        'required' => true,
        'objects' => $packages,
        'idSelect' => 'id_012',
        'nameSelect' => 'name_012'
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.cron_expression'),
        'name' => 'cronExpression',
        'value' => old('cronExpression', isset($object->cron_expression_011)? $object->cron_expression_011 : null),
        'maxLength' => '255',
        'rangeLength' => '9,255',
        'required' => true
    ])
    @if($action == 'update')
        @include('sergeant::includes.html.form_text_group', [
            'label' => trans('sergeant::sergeant.last_run'),
            'name' => 'lastRun',
            'value' => $lastRun,
            'readOnly' => true,
            'fieldSize' => 6
        ])
        @include('sergeant::includes.html.form_text_group', [
            'label' => trans('sergeant::sergeant.next_run'),
            'name' => 'nextRun',
            'value' => $nextRun,
            'readOnly' => true,
            'fieldSize' => 6
        ])
    @endif
    @include('sergeant::includes.html.form_checkbox_group', [
        'label' => trans('sergeant::sergeant.active'),
        'name' => 'active',
        'value' => 1,
        'checked' => old('active', isset($object->active_011)? $object->active_011 : false)
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.key'),
        'name' => 'key',
        'value' => old('key', isset($object->key_011)? $object->key_011 : null),
        'maxLength' => '50',
        'rangeLength' => '2,50',
        'required' => true
    ])
    <!-- /.sergeant::cron_jobs.form -->
@stop