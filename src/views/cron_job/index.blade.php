@extends('sergeant::layouts.index', ['newTrans' => 'new2'])

@section('head')
    @parent
    <!-- sergeant::cron_job.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [8,9]},
                        { "class": "checkbox-column", "targets": [8]},
                        { "class": "align-center", "targets": [5,9]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix)) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.sergeant::cron_job.index -->
@stop

@section('tHead')
    <!-- sergeant::cron_job.index -->
    <th data-hide="phone,tablet">ID.</th>
    <th data-class="expand">{{ trans('sergeant::sergeant.name') }}</th>
    <th data-hide="phone,tablet">{{ trans_choice('sergeant::sergeant.package', 1) }}</th>
    <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.key') }}</th>
    <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.cron_expression') }}</th>
    <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.active') }}</th>
    <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.last_run') }}</th>
    <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.next_run') }}</th>
    <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
    <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    <!-- /.sergeant::cron_job.index -->
@stop