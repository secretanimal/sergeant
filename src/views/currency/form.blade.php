@extends('sergeant::layouts.form')

@section('rows')
    <!-- sergeant::currency.create -->
    @include('sergeant::includes.html.form_text_group', [
        'fieldSize' => 2,
        'label' => 'ID',
        'name' => 'id_030',
        'value' => old('id_030', isset($object)? $object->id_030 : null),
        'readOnly' => true
    ])
    
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans_choice('sergeant::sergeant.alphabetic_code',1),
        'name' => 'alphabetic_code_030',
        'value' => old('alphabetic_code_030', isset($object)? $object->alphabetic_code_030 : null),
        'maxLength' => '12',
        'rangeLength' => '2,12',
        'required' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'fieldSize' => 6,
        'label' => trans('sergeant::sergeant.numeric_code'),
        'name' => 'numeric_code_030',
        'value' => old('alph_code_currency_033', isset($object)? $object->numeric_code_030 : null),
        'maxLength' => '4',
        'required' => true,
    ])
    @include('sergeant::includes.html.form_textarea_group', [
        'cols' => 1,
        'rows' => 3,
        'label' => trans_choice('sergeant::sergeant.name', 1),
        'name' => 'name_030',
        'value' => old('name_030', isset($object)? $object->name_030 : null),
        'maxLength' => '25',
        'required' => false
    ])
   @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.simbol'),
        'name' => 'simbol_030',
        'value' => old('simbol_030', isset($object)? $object->simbol_030 : null),
        'maxLength' => '25',
        'rangeLength' => '1,25',
        'required' => true
    ])
    <!-- /.sergeant::currency.create -->
@stop