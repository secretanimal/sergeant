<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body style="font-family: Lucida Sans Unicode, Bitstream Vera Sans, Trebuchet Unicode MS, Lucida Grande, Verdana, Helvetica, sans-serif; ">
        <div style="width: 80%;margin: auto;color: #333;border: 1px solid #ccc; border-radius: 4px; padding: 10px;">
            Hola {{ $name }}, Gracias por registrarse en RentSargeant, para comenzar a utilizar el sistema debe confirmar su correo electr&oacute;nico visitando el siguiente enlace:<br><br><br>
            
            <div style='text-align: center; width: 100%;'><a href="{{ URL::to(config('sergeant.appName') . '/sergeant/register/verify/' . $confirmation_code['confirmation_code']) }}" style="background-color: #00afa2;background-image: none;filter: none;border: 0;-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none;padding: 6px 13px;text-shadow: none;font-size: 14px;font-weight: normal;color: #ffffff; font-size: 13px; cursor: pointer; -webkit-border-radius: 0; -moz-border-radius: 0; border-radius: 4px; position: relative; z-index: 1;  -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none;font-weight: bold; text-decoration:none;"> Confirmar registro</a></div>

            <p style='font-family: Lucida Sans Unicode, Bitstream Vera Sans, Trebuchet Unicode MS, Lucida Grande, Verdana, Helvetica, sans-serif; font-size:12px;'>
                <br />Atte.
                <br />Equipo RentSargeant
            </p>
                    
            <p style='font-family: Lucida Sans Unicode, Bitstream Vera Sans, Trebuchet Unicode MS, Lucida Grande, Verdana, Helvetica, sans-serif; font-size:10px; color: #999; text-align: justify;'>
            
                    Este mensaje ha sido generado de forma autom&aacute;tica. Por favor, no responda este mensaje y sus documentos adjuntos
                    pueden contener informaci&oacute;n privilegiada y/o confidencial que est&eacute; dirigida exclusivamente a su destinatario. Si
                    usted recibe este mensaje y no es el destinatario indicado, por favor, notif&iacute;quelo inmediatamente y remita el
                    mensaje original a la direcci&oacute;n de correo electr&oacute;nico indicada. Cualquier copia, uso o distribuci&oacute;n no
                    autorizados de esta comunicaci&oacute;n queda estrictamente prohibida.
            </p>           


        </div>
    </body>
</html>