@extends('sergeant::layouts.index')

@section('head')
    @parent
    <!-- sergeant::email_account.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [3,4]},
                        { "class": "checkbox-column", "targets": [3]},
                        { "class": "align-center", "targets": [3,4]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix)) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.sergeant::email_account.index -->
@stop

@section('tHead')
    <!-- sergeant::email_account.index -->
    <tr>
        <th data-hide="phone,tablet">ID.</th>
        <th data-hide="expand">{{ trans('sergeant::sergeant.name') }}</th>
        <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.email') }}</th>
        <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
        <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    </tr>
    <!-- /.sergeant::email_account.index -->
@stop