@extends('sergeant::layouts.index')

@section('head')
    @parent
    <!-- sergeant::field.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [8,9]},
                        { "class": "checkbox-column", "targets": [8]},
                        { "class": "align-center", "targets": [5,6,7,9]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix), [base_lang()->id_001]) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.sergeant::field.index -->
@stop

@section('tHead')
    <!-- sergeant::field.index -->
    <tr>
        <th data-hide="phone,tablet">ID.</th>
        <th>{{ trans_choice('sergeant::sergeant.field_group', 1) }}</th>
        <th data-class="expand">{{ trans_choice('sergeant::sergeant.name', 1) }}</th>
        <th data-hide="phone">{{ trans_choice('sergeant::sergeant.field_type', 1) }}</th>
        <th data-hide="phone">{{ trans_choice('sergeant::sergeant.data_type', 1) }}</th>
        <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.required') }}</th>
        <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.sorting') }}</th>
        <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.maximum_length') }}</th>
        <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
        <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    </tr>
    <!-- /.sergeant::field.index -->
@stop