<!-- sergeant::field_value.breadcrumbs -->
<li>
    <a href="javascript:void(0)">{{ trans('sergeant::sergeant.package_name') }}</a>
</li>
<li>
    <a href="{{ route('customField', [base_lang()->id_001]) }}">{{ trans_choice('sergeant::sergeant.field', 2) }}</a>
</li>
<li class="current">
    <a href="{{ route($routeSuffix, ['field' => $field, 'lang' => $lang, 'offset' => $offset]) }}">{{ trans_choice($objectTrans, 2) }}</a>
</li>
<!-- /.sergeant::field_value.breadcrumbs -->