@extends('sergeant::layouts.index')

@section('head')
    @parent
    <!-- sergeant::field_value.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [5,6]},
                        { "class": "checkbox-column", "targets": [5]},
                        { "class": "align-center", "targets": [4,6]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix), ['field' => $field, 'lang' => base_lang()->id_001]) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.sergeant::field_value.index -->
@stop

@section('tHead')
    <!-- sergeant::field_value.index -->
    <tr>
        <th data-hide="phone,tablet">ID.</th>
        <th>{{ trans_choice('sergeant::sergeant.field', 1) }}</th>
        <th>{{ trans_choice('sergeant::sergeant.language', 1) }}</th>
        <th data-class="expand">{{ trans_choice('sergeant::sergeant.name', 1) }}</th>
        <th>{{ trans('sergeant::sergeant.featured') }}</th>
        <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
        <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    </tr>
    <!-- /.sergeant::field_value.index -->
@stop