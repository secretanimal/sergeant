<div @if(isset($containerId)) id="{{ $containerId }}"@endif class="form-group">
    <div class="col-md-{{ $fieldSize or 12 }} empty-form"></div>
</div>