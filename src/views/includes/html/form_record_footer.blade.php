<!-- sergeant::includes.html.form_record_footer -->
    <div class="form-actions">
        @if(isset($beforeButtonFooter))
            {!! $beforeButtonFooter !!}
        @endif
        @if($action != 'show')
            <button type="submit" class="btn margin-r10">{{ trans('sergeant::sergeant.save') }}</button>
        @endif
        @if($viewParameters['cancelButton'])
            <a id="cancel" class="btn btn-inverse" href="{{ route($routeSuffix, $urlParameters) }}">{{ trans('sergeant::sergeant.cancel') }}</a>
        @endif
        @if($action == 'update' && isset($lang) && $lang->id_001 != base_lang()->id_001)
            <a class="btn btn-danger margin-l10 delete-lang-record" data-delete-url="{{ route('deleteTranslation' . ucfirst($routeSuffix), $urlParameters) }}">{{ trans('sergeant::sergeant.delete_translation') }}</a>
        @endif
        @if(isset($afterButtonFooter))
            {!! $afterButtonFooter !!}
        @endif
    </div>
</form>
<!-- /.sergeant::includes.html.form_record_footer -->