<!-- sergeant::includes.js.delete_file -->
<script>
    $(document).ready(function() {
        $('.link-delete-file').on('click', function(){
            $.deleteFile(this)
        })
    })

    $.deleteFile = function (that)
    {
        $.msgbox('{{ trans('sergeant::sergeant.message_delete_file') }}', {
                type:'confirm',
                buttons: [
                    {type: 'submit', value: '{{ trans('sergeant::sergeant.accept') }}'},
                    {type: 'cancel', value: '{{ trans('sergeant::sergeant.cancel') }}'}
                ]
            },
            function(buttonPressed) {
                if(buttonPressed == '{{ trans('sergeant::sergeant.accept') }}')
                {
                    if($(that).data('href') == "")
                    {
                        $(that).closest('.filelink').hide()
                        $(that).closest('.file-container').find('.fileinput').fadeIn()
                        $("[name=" + $(that).data('name') + "]").val('')
                    }
                    else
                    {
                        $.ajax({
                            type: "POST",
                            headers:    {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            url: $(that).data('href'),
                            data:
                            {
                                id: $(that).data('id'),
                                file: $(that).data('file')
                            },
                            dataType: 'json',
                            success: function()
                            {
                                $(that).closest('.filelink').hide()
                                $(that).closest('.file-container').find('.fileinput').fadeIn()
                                $("[name=" + $(that).data('name') + "]").val('')
                            },
                            error: function(e)
                            {
                                alert('error')
                                console.log(e)
                            }
                        });
                    }
                }
            }
        )
    }
</script>
<!-- /.sergeant::includes.js.delete_file -->