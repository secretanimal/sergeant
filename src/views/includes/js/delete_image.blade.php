<!-- sergeant::includes.js.delete_image -->
<script>
    function deleteImage(url, inputHidden, inputFile, inputImage)
    {
        $.msgbox('{{ trans('sergeant::sergeant.message_delete_image') }}', {
                type:'confirm',
                buttons: [
                    {type: 'submit', value: '{{ trans('sergeant::sergeant.accept') }}'},
                    {type: 'cancel', value: '{{ trans('sergeant::sergeant.cancel') }}'}
                ]
            },
            function(buttonPressed) {
                if(buttonPressed == '{{ trans('sergeant::sergeant.accept') }}')
                {
                    $.ajax({
                        type: "POST",
                        headers:    {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        url: url,
                        dataType: 'text',
                        success: function()
                        {
                            if(typeof inputFile != 'undefined' && typeof inputImage != 'undefined')
                            {
                                $(inputImage).hide();
                                $(inputFile).fadeIn();
                            }
                            else
                            {
                                $('#inputImage').hide();
                                $('#inputFile').fadeIn();
                            }

                            if(typeof inputHidden != 'undefined')
                            {
                                $(inputHidden).val('');
                            }
                            else
                            {
                                $("[name='image']").val('');
                            }
                        },
                        error: function(e)
                        {
                            alert('error');
                            console.log(e);
                        }
                    });
                }
            }
        );
    }
</script>
<!-- /.sergeant::includes.js.delete_image -->