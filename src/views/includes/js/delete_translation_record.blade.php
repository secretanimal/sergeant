@if(isset($id))
    <!-- sergeant::includes.js.delete_translation_record -->
    <script>
        $(document).ready(function() {
            $('.delete-lang-record').on('click', function() {

                var that = this

                $.msgbox('{!! trans('sergeant::sergeant.message_delete_translation_record', ['id' => $id]) !!}',
                    {
                        type:'confirm',
                        buttons: [
                            {type: 'submit', value: '{{ trans('sergeant::sergeant.accept') }}'},
                            {type: 'cancel', value: '{{ trans('sergeant::sergeant.cancel') }}'}
                        ]
                    },
                    function(buttonPressed) {
                        if(buttonPressed=='{{ trans('sergeant::sergeant.accept') }}')
                        {
                            $(location).attr('href', $(that).data('delete-url'));
                        }
                    }
                )
            })
        })
    </script>
    <!-- /.sergeant::includes.js.delete_translation_record -->
@endif