<!-- sergeant::includes.html.script_header_form -->
<link rel="stylesheet" href="{{ asset('packages/rent/sergeant/vendor/jquery.select2/css/select2.css') }}">
<link rel="stylesheet" href="{{ asset('packages/rent/sergeant/vendor/jquery.select2.custom/css/select2.css') }}">

<script src="{{ asset('packages/rent/sergeant/plugins/uniform/jquery.uniform.min.js') }}"></script>
<script src="{{ asset('packages/rent/sergeant/plugins/fileinput/fileinput.js') }}"></script>
<script src="{{ asset('packages/rent/sergeant/vendor/jquery.select2.custom/js/select2.min.js') }}"></script>
<script src="{{ asset('packages/rent/sergeant/vendor/jquery.select2/js/i18n/' . config('app.locale') . '.js') }}"></script>

<!-- form validation -->
<script src="{{ asset('packages/rent/sergeant/vendor/jquery.validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('packages/rent/sergeant/vendor/jquery.validation/additional-methods.min.js') }}"></script>
@if(config('app.locale') != 'en')
    <script src="{{ asset('packages/rent/sergeant/vendor/jquery.validation/localization/messages_' . config('app.locale') . '.min.js') }}"></script>
@endif
<!-- /.form validation -->

<script>
    $(document).ready(function() {
        if ($.fn.inputlimiter) {
            $.extend(true, $.fn.inputlimiter.defaults, {
                boxAttach:  false,
                remText:    "{{ trans('sergeant::js-inputlimiter.remText') }}",
                limitText:  "{{ trans('sergeant::js-inputlimiter.limitText') }}",
                zeroPlural: true
            });
        }
        if ($.fn.fileInput) {
            // Set default options
            $.extend(true, $.fn.fileInput.defaults, {
                    placeholder: '{{ trans('sergeant::sergeant.file_not_select') }}...',
                    buttontext: '{{ trans('sergeant::sergeant.select') }}...',
                    inputsize: 'input-block-level'
            });
        }
    });
</script>
<!-- /.sergeant::includes.html.script_header_form -->