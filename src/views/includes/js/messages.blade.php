@if(session('msg') != null && session('msg') == 1)
    <!-- sergeant::includes.js.messages -->
    <script>
        $(document).ready(function() {
            new PNotify({
                type:   'success',
                title:  '{{ trans('sergeant::sergeant.action_successful') }}',
                text:   '{!! session('txtMsg') !!}',
                opacity: .9,
                styling: 'fontawesome'
            });
        });
    </script>
    <!-- /.sergeant::includes.js.messages -->
@endif

@if(session('msg') != null && session('msg') == 2)
    <!-- sergeant::includes.js.messages -->
    <script>
        $(document).ready(function() {
            new PNotify({
                type:   'error',
                title:  '{{ trans('sergeant::sergeant.action_error') }}',
                text:   '{!! session('txtMsg') !!}',
                opacity: .9,
                styling: 'fontawesome'
            });
        });
    </script>
    <!-- /.sergeant::includes.js.messages -->
@endif