@extends('sergeant::layouts.index')

@section('head')
    @parent
    <!-- sergeant::lang.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [6,7]},
                        { "class": "checkbox-column", "targets": [6]},
                        { "class": "align-center", "targets": [3,4,7]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix)) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.sergeant::lang.index -->
@stop

@section('tHead')
    <!-- sergeant::lang.index -->
    <th data-hide="phone,tablet">ID.</th>
    <th>{{ trans_choice('sergeant::sergeant.image', 2) }}</th>
    <th data-class="expand">{{ trans_choice('sergeant::sergeant.language', 2) }}</th>
    <th data-hide="phone">{{ trans('sergeant::sergeant.base_language') }}</th>
    <th data-hide="phone">{{ trans('sergeant::sergeant.active') }}</th>
    <th data-hide="phone">{{ trans('sergeant::sergeant.sorting') }}</th>
    <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
    <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    <!-- /.sergeant::lang.index -->
@stop