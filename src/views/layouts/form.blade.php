@extends('sergeant::layouts.record')

@section('content')
    <!-- sergeant::layouts.form -->
    @include('sergeant::includes.html.form_record_header')
        @yield('rows')
    @include('sergeant::includes.html.form_record_footer')

    @yield('outForm') <!-- popups forms inline -->
    <!-- /.sergeant::layouts.form -->
@stop