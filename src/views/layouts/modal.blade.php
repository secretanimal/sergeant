<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
        @section('title')
        <title>RENT SERGEANT</title>
        @show

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/vendor/jquery-ui/jquery-ui.min.css') }}">

        <!-- Theme -->
        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/css/plugins.css') }}">
        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/css/responsive.css') }}">
        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/css/icons.css') }}">
        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/css/custom/icons.css') }}">

        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/css/fontawesome/css/font-awesome.min.css') }}">

        <!-- Custom fonts -->
        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/css/font-rent/styles.css') }}">

        <!--[if IE 8]>
        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/css/ie8.css') }}">
        <![endif]-->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700">

        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/vendor/cssloader/css/cssloader.css') }}">
        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/plugins/jquery.msgbox/javascript/msgbox/jquery.msgbox.css') }}">

        <link rel="stylesheet" href="{{ asset('packages/rent/sergeant/css/custom/style.css') }}">

        <!--=== JavaScript ===-->
        <script src="{{ asset('packages/rent/sergeant/js/libs/jquery-2.1.3.min.js') }}"></script>
        <script src="{{ asset('packages/rent/sergeant/vendor/jquery-ui/jquery-ui.min.js') }}"></script>

        <script src="{{ asset('packages/rent/sergeant/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('packages/rent/sergeant/js/libs/lodash.compat.min.js') }}"></script>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="{{ asset('packages/rent/sergeant/js/libs/html5shiv.js') }}"></script>
        <![endif]-->

        <!-- Smartphone Touch Events -->
        <script src="{{ asset('packages/rent/sergeant/plugins/touchpunch/jquery.ui.touch-punch.min.js') }}"></script>
        <script src="{{ asset('packages/rent/sergeant/plugins/event.swipe/jquery.event.move.js') }}"></script>
        <script src="{{ asset('packages/rent/sergeant/plugins/event.swipe/jquery.event.swipe.js') }}"></script>

        <!-- General -->
        <script src="{{ asset('packages/rent/sergeant/js/libs/breakpoints.js') }}"></script>
        <script src="{{ asset('packages/rent/sergeant/plugins/respond/respond.min.js') }}"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
        <script src="{{ asset('packages/rent/sergeant/plugins/cookie/jquery.cookie.min.js') }}"></script>
        <script src="{{ asset('packages/rent/sergeant/vendor/slimscroll/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('packages/rent/sergeant/plugins/slimscroll/jquery.slimscroll.horizontal.js') }}"></script>

        <!-- App -->
        <script src="{{ asset('packages/rent/sergeant/js/app.js') }}"></script>
        <script src="{{ asset('packages/rent/sergeant/js/plugins.js') }}"></script>
        <script src="{{ asset('packages/rent/sergeant/js/plugins.form-components.js') }}"></script>

        <!-- JS OWN -->
        <!-- Loader -->
        <script src="{{ asset('packages/rent/sergeant/vendor/cssloader/js/jquery.cssloader.js') }}"></script>
        <script src="{{ asset('packages/rent/sergeant/plugins/jquery.msgbox/javascript/msgbox/jquery.msgbox.min.js') }}"></script>
        <script src="{{ asset('packages/rent/sergeant/js/custom/app.js') }}"></script>
        <!-- /JS OWN -->

        <script>
            $(document).ready(function()
            {
                $.cssLoader({
                    urlPlugin:  '/packages/rent/sergeant/vendor',
                    spinnerColor: '#4d7496',
                    theme: 'material'
                });

                App.init(); // Init layout and core plugins
                Plugins.init(); // Init all plugins
                FormComponents.init(); // Init all form-specific plugins
                SergeantApp.init() // Init custom plugins
            });
        </script>

        <!-- custom head -->
        @yield('head')
    </head>
    
    <body class="theme-dark">
        <div id="pre-cssloader"></div>

        <div id="container">
            <!--=== Page Header ===-->
            <div class="page-header-margin"></div>
            <!-- /Page Header -->
            <!--=== Page Content ===-->
            @yield('mainContent')
            <!-- /Page Content -->
        </div><!-- /.container -->
    </body>
</html>