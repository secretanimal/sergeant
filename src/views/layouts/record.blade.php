@extends(isset($extends) && is_array($extends) && isset($extends['record'])? $extends['record'] : 'sergeant::layouts.default')

@section('head')
    @parent
    @include('sergeant::includes.js.header_form')
@stop

@section('mainContent')
    <!-- sergeant::layouts.record -->
    <div class="row">
        <div class="col-md-12">
            <div class="widget box">
                <div class="widget-header">
                    <h4><i class="{{ isset($icon)? $icon : 'icomoon-icon-power' }}"></i> {{ isset($customTransHeader)? $customTransHeader : trans_choice($objectTrans, 1) }}</h4>
                </div>
                <div class="widget-content">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <!-- /.sergeant::layouts.record -->
@stop