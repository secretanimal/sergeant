@extends('sergeant::layouts.form')

@section('rows')
    <!-- sergeant::packages.create -->
    @include('sergeant::includes.html.form_text_group', [
        'label' => 'ID',
        'name' => 'id',
        'value' => old('id', isset($object)? $object->id_012 : null),
        'readOnly' => true,
        'fieldSize' => 2
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.name'),
        'name' => 'name',
        'value' => old('name', isset($object)? $object->name_012 : null),
        'maxLength' => '255',
        'rangeLength' => '2,255',
        'required' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans_choice('sergeant::sergeant.folder', 1),
        'name' => 'folder',
        'value' => old('folder', isset($object)? $object->folder_012 : null),
        'maxLength' => '50',
        'rangeLength' => '2,50',
        'required' => true
    ])
    @include('sergeant::includes.html.form_checkbox_group', [
        'label' => trans('sergeant::sergeant.active'),
        'name' => 'active',
        'value' => 1,
        'checked' => old('active', isset($object)? $object->active_012 : null)
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.sorting'),
        'name' => 'sorting',
        'value' => old('sorting', isset($object)? $object->sorting_012 : null),
        'type' => 'number',
        'fieldSize' => 2
    ])
    <!-- /sergeant::packages.create -->
@stop