@extends('sergeant::layouts.index', ['newTrans' => 'new2'])

@section('head')
    @parent
    <!-- sergeant::packages.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        {"sortable": false, "targets": [5,6]},
                        {"class": "checkbox-column", "targets": [5]},
                        {"class": "align-center", "targets": [3,6]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix)) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /sergeant::packages.index -->
@stop

@section('tHead')
    <!-- sergeant::packages.index -->
    <th data-hide="phone,tablet">ID.</th>
    <th data-class="expand">{{ trans('sergeant::sergeant.name') }}</th>
    <th>{{ trans_choice('sergeant::sergeant.folder', 1) }}</th>
    <th data-hide="phone">{{ trans('sergeant::sergeant.active') }}</th>
    <th data-hide="phone">{{ trans('sergeant::sergeant.sorting') }}</th>
    <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
    <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    <!-- /sergeant::packages.index -->
@stop