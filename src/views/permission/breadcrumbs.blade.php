<!-- sergeant::permission.breadcrumbs -->
<li>
    <a href="javascript:void(0)">{{ trans('sergeant::sergeant.administration') }}</a>
</li>
<li>
    <a href="{{ route($routeSuffixProfile, $offsetProfile) }}">{{ trans_choice('sergeant::sergeant.profile', 2) }}</a>
</li>
<li class="current">
    <a href="{{ route($routeSuffix, [$offset, $profile, $offsetProfile]) }}">{{ trans_choice($objectTrans, 2) }}</a>
</li>
<!-- /.sergeant::permission.breadcrumbs -->