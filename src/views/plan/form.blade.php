@extends('sergeant::layouts.form')

@section('rows')
    <!-- sergeant::plans.create -->
    @include('sergeant::includes.html.form_text_group', [
        'fieldSize' => 2,
        'label' => 'ID',
        'name' => 'id_033',
        'value' => old('id_033', isset($object)? $object->id_033 : null),
        'readOnly' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.name'),
        'name' => 'name_033',
        'value' => old('name_033', isset($object)? $object->name_033 : null),
        'maxLength' => '25',
        'rangeLength' => '2,25',
        'required' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans_choice('sergeant::sergeant.price',1),
        'name' => 'price_033',
        'value' => old('price_033', isset($object)? $object->price_033 : null),
        'min'   => '0.00',
        'max'   => '999999.99',
        'type'  => 'number',
        'maxLength' => '12',
        'rangeLength' => '2,12',
        'required' => true
    ])
    @include('sergeant::includes.html.form_select_group', [
        'fieldSize' => 6,
        'label' => trans('sergeant::sergeant.currency'),
        'name' => 'alph_code_currency_033',
        'value' => old('alph_code_currency_033', isset($object)? $object->alph_code_currency_033 : null),
        'maxLength' => '4',
        'required' => true,
        'objects' => $currency,
        'idSelect' => 'alphabetic_code_030',
        'nameSelect' => 'name_030'
    ])
    @include('sergeant::includes.html.form_textarea_group', [
        'cols' => 1,
        'rows' => 3,
        'label' => trans_choice('sergeant::sergeant.description', 1),
        'name' => 'description_033',
        'value' => old('description_033', isset($object)? $object->description_033 : null),
        'maxLength' => '25',
        'required' => false
    ])
   
    <!-- /.sergeant::plans.create -->
@stop