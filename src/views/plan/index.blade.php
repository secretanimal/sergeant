@extends('sergeant::layouts.index')

@section('head')
    @parent
    <!-- sergeant::plans.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [3,5]},
                        { "class": "checkbox-column", "targets": [3]},
                        { "class": "align-center", "targets": [3,5]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix)) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.sergeant::plans.index -->
@stop

@section('tHead')
    <!-- sergeant::plans.index -->
    <th data-hide="phone,tablet">ID.</th>
    <th data-class="expand">{{ trans('sergeant::sergeant.name') }}</th>
    <th data-hide="phone">{{ trans_choice('sergeant::sergeant.price',1) }}</th>
    <th data-hide="phone">{{ trans('sergeant::sergeant.currency') }}</th>
    <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.description') }}</th>
    <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
    <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    <!-- /.sergeant::plans.index -->
@stop