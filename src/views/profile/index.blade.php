@extends('sergeant::layouts.index')

@section('head')
    @parent
    <!-- sergeant::profiles.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [2,3]},
                        { "class": "checkbox-column", "targets": [2]},
                        { "class": "align-center", "targets": [3]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix)) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });

        function setAllPermissions(that)
        {
            $.msgbox('{!! trans('sergeant::sergeant.message_set_all_permissions') !!}',
            {
                type:'confirm',
                buttons: [
                    {type: 'submit', value: '{{ trans('sergeant::sergeant.accept') }}'},
                    {type: 'cancel', value: '{{ trans('sergeant::sergeant.cancel') }}'}
                ]
            },
                function(buttonPressed)
                {
                    if(buttonPressed=='{{ trans('sergeant::sergeant.accept') }}')
                    {
                        $(location).attr('href', $(that).data('all-permissions-url'));
                    }
                }
            );
        }
    </script>
    <!-- /.sergeant::profiles.index -->
@stop

@section('tHead')
    <!-- sergeant::profiles.index -->
    <tr>
        <th data-hide="phone,tablet">ID.</th>
        <th data-class="expand">{{ trans('sergeant::sergeant.name') }}</th>
        <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
        <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    </tr>
    <!-- /.sergeant::profiles.index -->
@stop