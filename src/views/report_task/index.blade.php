@extends('sergeant::layouts.index', ['newTrans' => 'new'])

@section('head')
    @parent
    <!-- sergeant::reports.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        {"sortable": false, "targets": [3,4]},
                        {"class": "checkbox-column", "targets": [3]},
                        {"class": "align-center", "targets": [4]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix)) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /sergeant::reports.index -->
@stop

@section('tHead')
    <!-- sergeant::reports.index -->
    <th data-hide="phone,tablet">ID.</th>
    <th data-class="expand">{{ trans('sergeant::sergeant.email') }}</th>
    <th data-hide="phone">{{ trans('sergeant::sergeant.subject') }}</th>
    <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
    <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    <!-- /sergeant::reports.index -->
@stop