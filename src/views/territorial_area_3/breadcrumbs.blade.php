<!-- sergeant::territorial_areas_3.breadcrumbs -->
<li>
    <a href="javascript:void(0)">{{ trans('sergeant::sergeant.administration') }}</a>
</li>
<li>
    <a href="{{ route('country', ['lang' => base_lang()->id_001, 'offset' => $parentOffset]) }}">{{ trans_choice('sergeant::sergeant.country', 2) }}</a>
</li>
<li class="current">
    <a href="{{ route($routeSuffix, $urlParameters) }}">{{ $country->territorial_area_3_002 }}</a>
</li>
<!-- /sergeant::territorial_areas_3.breadcrumbs -->