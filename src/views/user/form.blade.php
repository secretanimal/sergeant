@extends('sergeant::layouts.form')

@section('rows')
    <!-- sergeant::users.create -->
    @include('sergeant::includes.html.form_text_group', [
        'fieldSize' => 2,
        'label' => 'ID',
        'name' => 'id',
        'value' => old('id', isset($object)? $object->id_010 : null),
        'readOnly' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.name'),
        'name' => 'name',
        'value' => old('name', isset($object)? $object->name_010 : null),
        'maxLength' => '255',
        'rangeLength' => '2,255',
        'required' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.surname'),
        'name' => 'surname',
        'value' => old('surname', isset($object)? $object->surname_010 : null),
        'maxLength' => '255',
        'rangeLength' => '2,255',
        'required' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'fieldSize' => 6,
        'label' => trans('sergeant::sergeant.email'),
        'name' => 'email',
        'value' => old('email', isset($object)? $object->email_010 : null),
        'maxLength' => '255',
        'rangeLength' => '2,255',
        'required' => true,
        'type' => 'email'
    ])
    @include('sergeant::includes.html.form_select_group', [
        'fieldSize' => 4,
        'label' => trans_choice('sergeant::sergeant.language', 1),
        'name' => 'lang',
        'value' => old('lang', isset($object)? $object->lang_id_010 : null),
        'required' => true,
        'objects' => $langs,
        'idSelect' => 'id_001',
        'nameSelect' => 'name_001'
    ])
    @include('sergeant::includes.html.form_checkbox_group', [
        'label' => trans('sergeant::sergeant.access'),
        'name' => 'access',
        'value' => 1,
        'checked' => old('access', isset($object)? $object->access_010 : true)
    ])
    @include('sergeant::includes.html.form_section_header', [
        'label' => trans('sergeant::sergeant.data_access'),
        'icon' => 'icomoon-icon-users'
    ])
    @include('sergeant::includes.html.form_select_group', [
        'fieldSize' => 4,
        'label' => trans_choice('sergeant::sergeant.profile', 1),
        'name' => 'profile',
        'value' => old('profile', isset($object)? $object->profile_id_010 : null),
        'required' => true,
        'objects' => $profiles,
        'idSelect' => 'id_006',
        'nameSelect' => 'name_006'
    ])
    @include('sergeant::includes.html.form_text_group', [
        'fieldSize' => 6,
        'label' => trans_choice('sergeant::sergeant.user', 1),
        'name' => 'user',
        'value' => old('user', isset($object)? $object->user_010 : null),
        'maxLength' => '255',
        'rangeLength' => '2,255',
        'required' => true
    ])
    @include('sergeant::includes.html.form_text_group', [
        'fieldSize' => 6,
        'label' => trans('sergeant::sergeant.password'),
        'type' => 'password',
        'name' => 'password',
        'value' => old('password'),
        'maxLength' => '50',
        'rangeLength' => '4,50',
        'required' =>  ! isset($object)
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.repeat_password'),
        'type' => 'password',
        'name' => 'repassword',
        'value' => old('repassword'),
        'maxLength' => '50',
        'rangeLength' => '4,50',
        'fieldSize' => 6,
        'required' => ! isset($object)
    ])
    @include('sergeant::includes.html.form_section_header', [
        'label' => trans('sergeant::sergeant.plan'),
        'icon' => 'icomoon-icon-card'
    ])
    @include('sergeant::includes.html.form_select_group', [
        'fieldSize' => 4,
        'label' => trans_choice('sergeant::sergeant.plan', 1),
        'name' => 'plan',
        'value' => old('plan', isset($object)? $object->id_033 : null),
        'required' => false,
        'objects' => $plans,
        'idSelect' => 'id_033',
        'nameSelect' => 'name_033'
    ])
    
    <!-- /.sergeant::users.create -->
@stop
