@extends('sergeant::layouts.index')

@section('head')
    @parent
    <!-- sergeant::users.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        { "sortable": false, "targets": [6,7]},
                        { "class": "checkbox-column", "targets": [6]},
                        { "class": "align-center", "targets": [5,7]}
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix)) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.sergeant::users.index -->
@stop

@section('tHead')
    <!-- sergeant::users.index -->
    <th data-hide="phone,tablet">ID.</th>
    <th data-class="expand">{{ trans('sergeant::sergeant.name') }}</th>
    <th data-hide="phone">{{ trans('sergeant::sergeant.surname') }}</th>
    <th data-hide="phone">{{ trans('sergeant::sergeant.email') }}</th>
    <th data-hide="phone,tablet">{{ trans_choice('sergeant::sergeant.profile', 1) }}</th>
    <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.access') }}</th>
    <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
    <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    <!-- /.sergeant::users.index -->
@stop